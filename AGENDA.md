# Introduction Course to MongoDB
## Agenda

The following is the planned schedule for the courses.

| # | Topic                                                             | Date       |
|---|-------------------------------------------------------------------|------------|
| 1 |Introduction                                                       | dd/mm/yyyy |
| 2 |CRUD                                                               | dd/mm/yyyy |
| 3 |Schema Design and Performance                                      | dd/mm/yyyy |
| 4 |Aggregation Framework (+ Introduction to Replication and Sharding) | dd/mm/yyyy |
| 5 |Replication & Sharding (+ Backup & Recovery)                       | dd/mm/yyyy |
