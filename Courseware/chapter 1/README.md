# Introduction Course to MongoDB

## Chapter 1

### Slides

- Introduction: <https://goo.gl/2N6OTx>

### Summary [ita]

Le necessità di avere a disposizione degli strumenti che si adattassero alle richieste moderne di flessibilità, scalabilità e performance, unite al bisogno di avere dei sistemi sempre on-line e all'esistenza sempre più forte di team agile, ha spinto il panorama dei database a concentrarsi su altre tipologie di strutturazione e gestione dei dati.
Negli ultimi anni inoltre, abbiamo sempre più spesso a che fare con applicativi che hanno a che fare con una mole di dati enorme; dati che comunque il più delle volte non sono completamente strutturati, oppure semi-strutturati o polimorfici (ossia che possono appartenere a diversi tipi di dato).

Da tutte queste e altre considerazioni nasce il mondo NoSQL, che come vediamo è in realtà molto variegato e ogni sezione si differenzia per la struttura dei dati interni, che di conseguenza ne influenza "l'usabilità" nelle diverse situazioni.
Partiamo appunto dai *document*, e quindi database *document-oriented*, ossia orientati ai documenti.
MongoDB vediamo che fa parte di questa sezione, strutturando i dati in documenti BSON (che vedremo poi più avanti nel corso), ma di famoso c'è anche Couchbase, similie in certi versi a MongoDB.

Ma cos'è di fatto MongoDB?
MongoDB è un *datastore* (o database) non relazionale per documenti tipo JSON, abbastanza semplice da usare al livello programmazione, molto versatile, *schemaless* (che approfondiremo più avanti) e, ultimo ma non meno importante, open source.

Ok quindi, dove va a collocarsi il mondo di MongoDB?
Se pensiamo ai database in generale possiamo immaginare questi due assi: una riguarda la scalabilità e le performance, e l'altra riguarda le funzionalità offerte.
Pensiamo ad esempio ai key/value store, questo tipo di DB è molto scalabile ma offre pochissime funzionalità; all’altro estremo abbiamo i normali RDBMS che offrono un notevole numero di funzionalità come le Join, le transazioni, i Trigger, e così via, a discapito della scalabilità (pensiamo ad esempio a soluzioni come Oracle).
La domanda è: riusciamo a trovare qualcosa che abbia una buona collezione di funzionalità ma senza andare a discapito della scalabilità e delle performance?
La risposta è MongoDB; il delta che lo separa dai normali RDBMS, e che quindi permette di salire sull'asse della scalabilità, è dovuto all'assenza di determinate funzionalità come le Join e le Transaction.
Per via della natura Document-Oriented di MongoDB in realtà le Join non si rendono particolarmente necessarie, per questo motivo non ci sono (native) su MongoDB; inoltre di fatto sono le operazioni forse più pesanti in termini di performance (scalano poco).
MongoDB non supporta le transazioni; hai accesso atomico al documento e quello che succede spesso è che ci sia una certa gerarchia all’interno del documento stesso, ma se vuoi fare degli update su più documenti e hai bisogno di una transazione, puoi eventualmente decidere di farla "a mano" sul codice, rimandando appunto l'implementazione al programmatore o ingegnerie che costruisce il sistema.

Ma passiamo ora a uno dei concetti sui quali si basa l'intera filosofia di MongoDB: *The Nexus Architecture*.
Il focus principale sta nella volontà di combinare le funzionalità critiche dei database relazionali con l'innovazione delle tecnologie NoSQL.
È importante tenere a mente che colossi come Oracle hanno fatto sicuramente un ottimo lavoro con i loro database, per cui è sicuramente essenziale mantenere funzionalità come **linguaggi di interrogazione espressivi**, in quanto gli utenti devono essere in grado di accedere e manipolare i loro dati in maniera sofisticata e potente, utilizzando query, proiezioni, aggregati e operazioni di aggiornamento, in modo da supportare i loro applicativi.
Gli **indici secondari** ricoprono un ruolo fonbdamentale nell'accesso *intelligente* ai dati, sia in lettura che in scrittura, supportati in maniera nativa dai database senza "scomodare" ingegneri e programmatori sul codice.
Una **forte consistenza** dei dati è anche una caratteristica dei RDBMS da mantenere intatta, in quanto gli utenti e i vari applicativi devono necessariamente poter leggere lo stesso dato scritto magari in precedenza, senza che questo abbia subito modifiche *casuali*.
Per finire, una buona **gestione e integrazione** al livello enterprise è, ovviamente, fondamentale.

Ok, ora che abbiamo capito quali elementi riutilizzare dal mondo dei database relazionali, cosa possiamo introdurre nella nostra architettura per essere al passo dei requisiti imposti dai moderni applicativi che non vengono soddisfatti dai sistemi relazionali?
Sicuramente la **flessibilità**, e di conseguenza un **modello flessibile di dati**, sono cruciali per rendere estremamente facile salvare e combinare dati di ogni struttura, permettendo modifiche dinamiche degli schemi utilizzati senza *downtime*, quindi non interrompendo l'esecuzione. Questo è sicuramente uno dei caposaldi del mondo NoSQL.
Una **scalabilità elastica** risulta molto interessante; i nuovi sistemi NoSQL sono stati appunto costrutiti avendo in mente la scalabilità dei sistemi stessi, i quali includono in maniera nativa funzionalità come *sharding* e *partizionamento* (o partitioning), permettendo ai database di scalare orizzontalmente su più server sia locali che sul cloud, teoricamente con una crescrita quasi illimitata (ci sono ovviamente dei limiti).
Le **performance** associate ne guadagnano ovviamente in termini di *throughput* e *latenza*, grazie anche alla facilità di accesso dei dati.
La possibilità di avere *downtime* pressochè nulli, unita ad alcuni nuovi servizi *database-as-a-service*, permettono agli utenti di avere un sistema **always on** con **deploy globali**.

MongoDB rappresenta quindi uno dei pochi database che ingloba le innovazioni del mondo NoSQL pur continuando a mantenere alcune delle basi dei database relazionali.

Tralasciamo ora questi aspetti per concentrarci un pò sulle basi e su come MongoDB salva i dati; lo facciamo parlando della rappresentazione a *documento*, del JSON e quindi di BSON.
Cominciamo appunto con il concetto di **documento**.
Abbiamo analizzato prima cos'è MongoDB e come differisce in parte dal mondo dei database relazionali; sappiamo dunque che MongoDB tralascia funzionalità come *join* e *transazioni*, forzandoci a migrare verso un nuovo modello di dati.
Tuttavia non possiamo perdere in efficacia e potenza, per cui abbiamo comunque bisogno di qualcosa che abbia lo stesso quantitativo di *potenza* ma che sia allo stesso tempo abbastanza malleabile da adattarsi facilmente allo stile di programmazione e ai moderni applicativi del giorno d'oggi.
Dimentichiamo quindi il concetto di *tupla* per muoverci completamente verso il modello *JSON Document*; questo modello introduce sicuramente tutta una serie di vantaggi come la facilità d'uso, la possibilità per un docmento di contenerne un altro al suo interno (*embedding*), il vantaggio della denormalizzazione dei dati (non abbiamo più bisogno di spacchettare un oggetto su più tabelle per rappresentarlo al meglio).

"Documento" quindi, ma abbiamo anche accennato al **JSON**.
Acronimo di *JavaScript Object Notation*, rappresenta, seguendo Wikipedia, il "formato adatto all'interscambio di dati fra applicazioni client-side".
Non c'è molto altro da dire in quanto, come immagino, tutti voi conosciate il JSON; tuttavia quello che ci interessa in questo momento è capire come sia possibile *rappresentare* un documento all'interno di un oggetto JSON e perchè questo formato sia stato scelto dagli ingegneri di MongoDB come base di partenzé come modello di salvataggio dei dati sul loro database.
Per rispondere al primo quesito ci basta analizzare un qualsiasi JSON ben formattato come quello nelle slide, dove vediamo chiaramente il concetto di *chiave-valore* che sta un pò alla base di questo formato, insieme ai vari tipi di dato che possiamo inserire (stringa, numero, array, binario, documento).
La risposta invece al perchè è stato scelto come base per la rappresentazione dei dati in MongoDB la possiamo trovare in due fatti: innanzitutto è un modellé relativamente semplice da usare e da interpretare, tutti quanti immagino abbiate capito che questo JSON rappresenti un libro o una pubblicazione in genere, con il proprio titolo, autore e così via; secondo punto invece ma comunque non meno importate è che JSON è indipendente dal linguaggio usato (RFC 7159), separando quindi i dati dal codice.

I dati in MongoDB vengono quindi salvati in JSON, ma siamo proprio sicuri che questo sia il formato reale?
Ovviamente si, ma c'è da fare un'ulteriore considerazione: come possiamo salvare il JSON che di fatto è un formato di dati, su disco, e quindi in termini di zero e uno?
È qui che ci viene in aiuto il **BSON**.
Il BSON, o *Binary JSON*, è uno standard aperto per la rappresentazione binaria di JSON, adatto quindi al salvataggio su qualunque supporto digitale.
L'idea principale che sta dietro al BSON è quella di avere la possibilità di fare **Fast Scannability**, ossia permettere di passare da una sezione all'altra dell'oggetto in maniera rapida, oltre ovviamente alla possibilità di salvare diversi tipi di dato; in pratica due dei concetti principali di JSON.
Concentriamoci sulla prima: ogni volta che facciamo accesso a un BSON non vogliamo fare una lettura sequenziale per andare a cercare il campo che ci interessa, ma possiamo andare diretti alla sezione di oggetto che ci interessa, questo perchè conosciamo quanto *pesa* ogni singolo campo, per cui sappiamo quanto dobbiamé *saltare*.
L'esempio che vedete sulle slide spiega la rappresentazione di questo piccolo oggetto JSON che abbiamo a disposizione, in modo da spiegare anche il concetto di *peso* per ogni sezione.
Ogni oggetto BSON comincia con la sua dimensione totale, ossia nei primi 32 bit (4 byte) c'è salvata la dimensione dell'oggetto; subito dopo abbiamo il tipo del primo campo, intero in questo caso (`int32`), espresso su 8 bit (1 byte), seguito dalla sezione contenente il nome, o chiave, del campo su una stringa di 16 bit (2 byte) terminata dal carattere `\0`, seguita a sua volta dalla sezione contenente il valore della chiave, in questo caso un intero su 32 bit (4 byte).
Il secondo paio chiave-valore è rappresentato analogamente, con la sola differenza che in questo caso il valore è appunto una stringa, per cui avremo i soliti 8 bit per il tipo, 16 bit per il nome della chiave, la sezione con il valore della stringa e relativo terminatore, il tutto chiuso dal terminatore del BSON.
Il tipo stringa è ovviamente un pò più complesso da utilizzare ma, in favore della *fast scannability* menzionata in precedenza, abbiamo appunto questo campo ulteriore che rappresenta la lunghezza della stringa stessa, in modo da favorire un'accesso settoriale all'oggetto.

Andiamo ora un pò più nel dettaglio sul funzionamento del BSON in ambito applicativo.
Ipotizziamo una generica architettura client-server, dove per server abbiamo un'istanza di MongoDB, mentre come client abbiamo una nostra applicazione, scritta in Java per esempio.
Dunque abbiamo visto come MongoDB rappresenta i propri dati, o documenti, sul disco del server formato BSON.
Mettiamo a questo punto che il client richieda un determinato documento; questo documento in formato BSON viene dunque trovato e inviato al client tramite una connessione (diciamo TCP, instaurata precedentemente) mantenendo lo stesso formato BSON (BSON across the wire).
Questo oggetto BSON arriva dunque fino al client praticamente immutato nello stesso formato BSON; una volta qui avremo sicuramente un driver MongoDB (che approfondiremo in seguito) che si preoccuperà di ricevere l'oggetto BSON e *trasformarlo* in qualcosa che sia utile e comprensibile nella nostra applicazione, diciamo un oggetto Java nel nostro caso.
Possiamo dunque pensare al BSON come un **formato di serializzazione**, che ci permette di trasformare, o *serializzare* un oggetto BSON in qualsiasi altro oggetto utile al nostro programma, come un oggetto Java ad esempio.
Una cosa simile la fa il processo `mongoimport` che vedremo successivamente, il quale trasforma il contenuto di un file in dati BSON.

Abbiamo parlato in precedenza di *flessibilità* e *modello flessibile di dati*, ma cosa vuol dire esattamente in MongoDB?
In MongoDB abbiamo il desiderio di avere questa nozione di **schemaless** (senza schema), tuttavia come forse alcuni di voi sanno già e comunque come vedremo più avanti, il concetto di *schema design* è assolutamente cruciale durante le fasi di creazione di una architettura basata su MongoDB.
La domanda quindi è: dove sta l'inghippo?
Semplice: proprio nella parola *schemaless*.
I documenti salvati in MongoDB tramite BSON hanno una struttura ben definita di chiave-valore e rappresentazione binaria; tuttavia questa può cambiare nel tempo, per cui sarebbe molto più accurato parlare di **dynamic schema**.
Una possibile analogia a quanto detto possiamo trovarla nel mondo della programmazione: pensiamo dunque a Java o il C che sono dei linguaggi fortemente tipizzati (tipizzazione statica) e paragoniamoli a linguaggi come JavaScript, PHP o Groovy, dove il tipo delle variabili dichiarate può cambiare dinamicamente a seguito di manipolazioni esterne (tipizzazione dinamica).
Tutto questo ci porta finalmente al concetto di flessibilità; i documenti salvati in MongoDB possono tranquillamente variare la loro struttura, o schema, i campi possono variare da documento a documento in una collezione e non c'è alcun bisogno di dichiararne la struttura al sistema (a MongoDB).
L'aggiunta di un campo a un documento può essere eseguita senza bisogno di modificare tutti gli altri documenti di una stessa collezione, senza aggiornare il sistema centrale e senza riavviare l'intero sistema.
Gli sviluppatori e ingegneri possono cominciare a scrivere codice e salvare gli oggetti creati dalla loro nuova applicazione; quando si decide di ampliare il sistema con nuove funzionalità, MongoDB continua a salvare eventuali oggetti aggiornati senza il bisogno di effettuare pesanti operazioni tipo `ALTER TABLE` o, peggio ancora, dover ripensare da capo il design della loro architettura di MongoDB.
MongoDB dunque si presta molto facilmente al nuovo concetto di programmazione agile, adattandosi quindi alle molte iterazioni che un team di sviluppo applica al proprio software; inoltre MongoDB favorisce quello che potremmo definire *polimorfismo dei dati*.
Tutto questo è molto interessante e sicuramente gli schemi dinamici portano una grande "agilità" durante lo sviluppo, ma che succede se la qualità dei dati è in realtà un punto cruciale del nostro applicativo?
Questo è senza dubbio importante e in alcuni casi non possiamo pensare di delegare tutto il lavoro a chi pensa e realizza le API del nostro sistema; una soluzione a questo "problema" ci viene fornita nelle ultime versioni di MongoDB tramite la funzionalità definita **Documment Validation**.
Semplicemente MongoDB ci da la possibilità di effettuare la validazione dei documenti salvati nelle nostre collezioni, tramite dei controlli forzati sulla struttura, sul tipo dei dati, sui range di alcuni valori e sulla presenza obbligatoria di alcuni campi.
(Vi invito a dare un'occhiata al [manuale ufficiale di MongoDB](https://docs.mongodb.com/manual/core/document-validation/) per ulteriori dettagli su questa parte)

Prima di passare a una fase un pò più pratica procediamo a rivedere un pò quali sono i requisiti di sistema che abbiamo bisogno per installare e far partire MongoDB sul nostro sistema.
MongoDB non ha bisogno di grosse risorse per funzionare correttamente, per cui sicuramente ognuna delle macchine che abbiamo a disposizione può andar bene, ma per essere più precisi consiglio una macchina con processore 64 bit (MongoDB gira anche su 32 bit ma è molto limitato in termini di capacità e velocità), un paio di giga di RAM e almeno un 30-40 giga tanto per stare sicuri; non faremo chissà quale prova adesso ma queste possono comunque essere usate come linee guida per crearvi una macchina virtuale sulla quale provare la vostra istanza di MongoDB.
Ovviamente quando poi vi troverete a dover mettere in piedi un grosso sistema basato su MongoDB dovrete poi tenere in considerazione alcuni accorgimenti sulla messa in produzione che trovate nella [sezione dedicata del manuale ufficiale di MongoDB](https://docs.mongodb.com/manual/administration/production-notes/#production-notes).
Indipendentemente dal sistema operativo che andrete a utilizzare, MongoDB gira praticamente quasi su tutti i sistemi e il manuale ufficiale riporta un [paragrafo dedicato](https://docs.mongodb.com/manual/installation/#tutorials) con tutte le guide da seguire passo passo per il sistema operativo che andremo a utilizzare.
Non sto a riportarvi in questa guida tali passaggi, piuttosto vi invito a migrare un attimo sul manuale ufficiale (che scopriremo essere una risorsa valida e ben fatta) per installare finalmente la nostra istanza di MongoDB.
Al termine dell'installazione possiamo quindi far partire la nostra istanza di MongoDB, stando accorti a creare prima di tutto una cartella dedicata dove MongoDB andrà fisicamente a istanziare i file BSON (altrimenti dovremo specificare ogni volta la cartella tramite il comando `--dbpath`).

Bene, ora che abbiamo installato MongoDB sulla nostra macchina e abbiamo testato che funziona, abbiamo uno strumento in particolare per tutte le questioni di tipo amministrativo riguardanti MongoDB: la shell `mongo`.
Questo è sicuramente il tool di amministrazione per eccellenza e in particolare vi consiglio di provare a usarlo soprattutto per quelli che saranno i comandi relativi alla gestione delle varie collezioni, indici secondari, sharding e cosi' via, ossia le caratteristiche un pò più avanzate (soprattutto se siete dei DBA).
In seguito vi consiglio di dare un'occhiata ai vari tool grafici supportati da MongoDB, i quali vi mettono a disposizione varie funzionalità *visuali* sicuramente più comode della shell; potete trovare alcuni di questi strumenti sul sito [MongoDB Tools](http://mongodb-tools.com/).
Ma torniamo alla shell.
La shell si avvia da linea di comando digitando semplicemente `mongo`; usa la sintassi di JavaScript e di fatto è un interprete interattivo di Javascript che viene usato per connetterci e interagire con l'instanza MongoDB.
Questo vuol dire che all'interno della shell utilizziamo comandi in stile JavaScript e anzi potete tranquillamente utilizzare questo linguaggio e, se preferite, importare direttamente script JavaScript per essere eseguiti.
Proviamo quindi a eseguire un paio di comandi:

- `for (i = 0; i < 3; i++) print("Hello, MongoDB developer!");`
- `x = 1`
- `y = 'abc'`
- `z = {a: 1}`
- `z.a`
- `z["a"]`

Tutte le operazioni fatte attraverso questa shell, come anche sui nostri futuri applicativi tramite gli appositi driver, esistono e vengono eseguite come metodi e funzioni tramite le normali API dei linguaggi di programmazione, non come linguaggio separato.
Cosa vuol dire esattamente?
Come abbiamo notato nella shell viene usato di fatto un linguaggio JavaScript.
MongoDB quindi non ha un vero e proprio linguaggio di interrogazione e interazione tipo SQL; ci sono alcuni DB non relazionali come ad esempio [Couchbase](http://www.couchbase.com/nosql-databases/couchbase-server) che mettono a disposizione dei *query tool* che molto si avvicinano al classico SQL che conosciamo tutti, ma che di fatto rappresenta solo una possibile (e forse nemmeno troppo utile) aggiunta.
MongoDB invece si dissocia in parte da questa forzatura e anzi ne fa uno dei suoi punti di forza maggiori, in quanto un generico programmatore si troverà sicuramente più a suo agio a interrogare e interagire con i normali oggetti della programmazione invece di dover passare necessariamente per un altro linguaggio alle volte non troppo semplice da utilizzare.

Ok, ora che abbiamo aperto la shell e ci siamo connessi al DB notiamo subito che la prima connessione di default è quella al DB `test`.
Ecco, già che ci siamo, diciamo subito che un'istanza di MongoDB di fatto può contenere più database, come magari fa MySQL; `test` è uno di quelli.
Notiamo quindi che la variabile `db` contiene appunto il nome del database al quale siamo connessi adesso.
Se vogliamo passare da un database all'altro è sufficiente usare il comando `use` seguito dal nome del database al quale vogliamo collegarci, anche se questo non è ancora stato creato (es. `use mongo-intro`).
Per tutti gli altri comandi vi consiglio di fare riferimento al comando `help`, il quale come vediamo può anche essere applicato al livello dei metodi, per cui abbiamo per esempio `db.help()` e cosi' via.
Bene, siamo quasi alla fine di questo capitolo ma prima di chiudere possiamo sicuramente fare due prove con la shell, magari facendo un pò di lettura e scrittura.
Ecco una lista di comandi che possiamo inserire:

- `doc = { "name" : "Marco" , "surname" : "Terrinoni" , "age": 26 , "profession" : "Engineer" }`
- `db`
- `db.people.insert(doc)`
- `db.people.find()` qui possiamo notare il campo `"_id"`, chiave primaria immutabile del documento
- `db.people.insert({"name": "John", "surname": "Doe", "age": 30, "profession": "Hacker"})`
- `db.people.find()`

Concludiamo questo capitolo proponendo una breve ma interessante panoramica su quale potrebbe essere un'architettura di una semplicissima applicazione scritta, ad esempio, in Java.
Innanzitutto abbiamo il nostro database MongoDB, che di fatto risiede nel processo `mongod`; abbiamo visto che possiamo connetterci al DB usando direttamente la shell di MongoDB, la quale si connette via TCP direttamente al processo `mongod`.
Come abbiamo detto però questa è l'architettura di un'applicazione quindi facile che la shell non la useremo attivamente tramite il codice.
Abbiamo quindi la nostra applicazione, con il nostro codice e magari altri framework che ci facilitano il compito di sviluppo; questa Java application conterrà un blocco fondamentale che è appunto il MongoDB Java Driver, il quale ci permette di fare più o meno le stesse operazioni che facciamo tramite shell, usando ovviamente le API fornite dal driver stesso.
Il driver instaura una connessione TCP simile a come fa la shell e comunica con l'instanza `mongod`.
Tutto questo ovviamente gira sulla JVM.
Ipotizziamo poi che la nostra applicazione esponga una porta 8080 magari accessibile via browser all'esterno, in modo da permettere ad eventuali utenti di interaggire con il programma.
Ed ecco qua semplicemente come si presenta un'architettura molto basilare ma abbastanza semplice da essere compresa al volo; notiamo tra l'altro che seppur con minimi cambiamenti, questa architettura può di fatto adattarsi a qualsiasi framework o linguaggio (Node.js tra i più evidenti).
Ci sono ovviamente altri concorrenti che possono entrare in gioco nella nostra architettura, in particolare che ci possono aiutare nella mappatura tra oggetti e documenti su DB ([Morphia](http://mongodb.github.io/morphia/) ad esempio).