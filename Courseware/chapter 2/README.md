# Introduction Course to MongoDB

## Chapter 2

### Slides

- CRUD: <http://goo.gl/hBbFtZ>

### Summary [ita]

Riprendiamo questo nostro percorso su MongoDB e NoSQL tornando un attimo sui nostri passi e discutiamo ancora un attimo sul concetto di CRUD e su come MongoDB permette l'accesso e la modifica ai dati salvati.

Sicuramente tutti voi avete almeno una volta sentito l'acronimo CRUD, che sta appunto per create-read-update-delete.
Possiamo di fatto considerarlo come uno standard per la manipolazione dei dati e se pensiamo ad un'ipotetica web application che gestisca dei dati, forse una delle prime cose alle quali possiamo pensare è a come questa nostra app inserisca, legga, modifichi e cancelli i dati che tiene in pancia.
Tutto questo ovviamente lo ritroviamo in MongoDB e, in particolare possiamo fare un breve sunto per precisare un attimo meglio la nomenclatura, in modo tale da
favorire anche chi di voi viene da un mondo prevalentemente segnato dalle tecnologie SQL.

|CRUD  |MongoDB|SQL   |
|------|-------|------|
|Create|Insert |Insert|
|Read  |Find   |Select|
|Update|Update |Update|
|Delete|Remove |Delete|

Questa tabella tuttavia potrebbe trarre in inganno e far pensare che MongoDB in realtà abbia una specie di alias per il normale linguaggio di interrogazione e manipolazione dati SQL.
MongoDB infatti non ha un suo *query language* proprietario, ossia non ha un simil-SQL che permette di scrivere query in senso stretto; tutte le operazioni CRUD di MongoDB infatti esistono solo ed esclusivamente come metodi e funzioni accessibili tramite le varie API dei diversi linguaggi di programmazione (a loro volta accessibili tramite i driver).
Questo è un concetto che già abbiamo considerato è in realtà molto importante perchè pone MongoDB su un piano diverso agli altri database, fornendo strumenti sicuramente molto utili aé programmatore che non deve stare a costruire query complicate in chissà quale linguaggio o dialetto proprietario dei vari DBMS.
Il tutto ovviamente mantenendo la stessa "potenza" del normale codice SQL.
La domanda che potremmo farci a questo punto allora è: ma come è possibile che MongoDB riesce a gestire comunque tutte le operazioni richieste senza avere un proprio linguaggio SQL e solo tramite invocazione di metodi e funzioni?
In realtà quello che succede "sotto al cofano" non è molto evidente e, in realtà, non è nemmeno poi tanto rilevante da una prospettiva squisitamente di programmazione; in pratica tutte queste operazioni che stiamo considerando si basano sul cosiddetto **Wire Protocol**.
Tutte le richieste di manipolazione dati ricevute tramite questi famosi metodi esposti dai driver comuninicano con l'istanza di MongoDB tramite questo protocollo, ossia un insieme di informazioni e definizioni di messaggi che permettono a MongoDB di capire esattamente le richieste inoltrate ed applicarle sulla propria base dati.
Il *MongoDB Wire Protocol* è un semplice protocollo request-response basato su socket, dove i vari client (shell, driver) comunicano con l'istanza del database attraverso una regolare connessione TCP.
Anche se molto interessante, non è questo tuttavia l'obiettivo principale di questo capitolo, per cui rimandiamo semplicemente alla [sezione della documentazione ufficiale](https://docs.mongodb.com/manual/reference/mongodb-wire-protocol/) nel caso vogliate approfondire ulteriormente questo discorso.

Piccola nota a margine: se siete interessati ad avere un DB NoSQL ma volete comunque tenere la possibilità di utilizzare un linguaggio simil-SQL, allora potete dare un'occhiata a Couchbase e il suo [N1QL](http://www.couchbase.com/n1ql)

Ok, ora che conosciamo meglio la struttura e il funzionamento della shell mongo procediamo con la spiegazione di alcuni dei comandi fondamentali della shell.
Cominciamo ovviamente con l'inserimento di documenti sul nostro database.
In MongoDB, come abbiamo spiegato nel capitolo precedente, i documenti sono rappresentati come oggetti JavaScript; possiamo quindi scrivere sulla shell il nostro oggetto in questo modo;

```JavaScript
var doc = { "name" : "Marco" , "age" : 26 , "profession" : "Software Engineer" }`;
```

Mettiamo subito in pratica questa cosa e avviamo prima un'istanza di MongoDB (`mongod`) e poi apriamo la shell (`mongo`); inseriamo la dichiarazione della variabile `doc` così come abbiamo appena fatto, in modo da salvare la variabile nell'ambiente di shell.
Dobbiamo inserire questo documento all'interno del database di MongoDB, per cui avremo sicuramente bisogno di un handler per interagire col DB.
Nemmeno a farlo apposta, la shell mongo ci mette a disposizione un handle attraverso la variabile `db`, la quale contiene anche il nome al DB corrente (semplicemente scrivendo `db` nella shell e premento invio).
Nel caso volessimo vedere quali altri database ci sono oltre a quello di default possiamo semplicemente usare il comando `show dbs`, il quale ci mostrerà la lista dei database disponibili attualmente in MongoDB.
Possiamo creare un nuovo database semplicemente "spostandoci" dentro usando il comando `use <nome_db>`; spostandoci dentro un database ancora non esistente di fatto equivale a crearne uno nuovo, dentro il quale possiamo creare nuove collezioni e inserire nuovi documenti (questo in realtà è parzialmente vero in quanto il database viene fisicamente creato solo quando un nuovo documento viene inserito in una collezione).
Ultima nota: possiamo vedere la lista delle collezioni all'interno di un database semplicemente usando il comando `show collections`.
Torniamo ora all'inserimento; precisiamo adesso che i documenti dentro MongoDB vivono in *collezioni*, che di fatto sono semplici *set di documenti*.
Da un punto di vista della programmazione (e dell'utilizzo della shell) vediamo che una collezione è vista come una proprietà del database (e quindi del `db`), per cui abbiamo che `db.people.insert(doc)` è il comando finale che dobbiamo utilizzare per inserire il documento definito in partenza all'interno della collezione `people` che risiede nel database corrente (`test` se non l'abbiamo cambiato prima).
Il metodo `insert` è quindi il metodo per fare l'inserimento in una collezione e di fatto fa proprio parte della collezione e prende in input un oggetto JavaScript.
Eseguiamo questo comando nella shell e per il momento saltiamo il messaggio di risultato che ci viene fuori dopo l'effettivo inserimento; spiegheremo il significato di WriteResult in un secondo momento ma per adesso vi basta sapere che il numero all'interno dell'oggetto di output rappresenta il numero di inserimenti effettuati.
Cerchiamo però il risultato dell'inserimento in un altro modo, se vogliamo più semplice ed eseguiamo il comando `find`, che vedremo maggiormente nel dettaglio a breve.
Anche la `find` è un metodo relativo alla collezione e se eseguito senza alcun parametro ritorna semplicemente la lista di tutti i documenti salvati all'interno della collezione (paginati tramite un cursore nel caso in cui il numero di documenti sia molto grande).
Eseguiamo quindi il comando `db.people.find()` ed ecco che possiamo vedere il nostro documento appena inserito; notiamo subito che ritroviamo sicuramente i campi inseriti in precedenza (i.e. `name`, `age`, `profession`), ma soprattutto vediamo questo campo in più con chiave `_id` e valore composto da un oggetto `ObjectId`; questo campo rappresenta di fatto l'Id del documento appena inserito.
Questo campo viene creato ogni volta che un nuovo documento viene inserito in una collezione, in quanto l'istanza server di MongoDB richiede che tutti i documenti siano indicizzati da un campo univoco.
Il campo `_id` è di fatto la *primary key* del documento e il suo valore deve essere univoco all'interno della collezione.
è importante dire che, a differenza di altri database, MongoDB non ci lascia cambiare il valore della chiave `_id` di un documento in quanto il suo valore è immutabile; possiamo simulare tale cambiamento in un documento cancellandolo e immediatamente reinserirlo nella collezione con un valore di chiave diverso ma questa non sarebbe un'operazione atomica e in generale non è una buona mossa.
Parliamo un attimo di questo **ObjectId** e anticipiamo qualcosa sulla gestione degli indici.
Abbiamo appena detto che MongoDB richiede che il campo Id di un documento sia univoco all'interno della collezione; MongoDB ci mette a disposizione questo tipo *ObjectId* per facilitarci la creazione di un indice univoco (una specie di *uuid* per essere precisi), il quale genera appunto un valore esadecimale derivato dai seguenti elementi: istante nel quale viene creato l'Id, identificativo della macchina che lo genera, Id del processo di MongoDB sulla stessa macchina e un counter globale sempre dello stesso processo.
In questo modo abbiamo un'ottima probabilità di non incorrere in collisioni tra Id della stessa collezione.
In fase di inserimento di un documento in una collezione quindi, se il campo `_id` non viene specificato, allora MongoDB genererà automaticamente sia il campo che il valore, inserendo appunto una chiave di tipo ObjectId.
Diciamo che in generale quanto appena detto ci basta per fare inserimenti di nuovi documenti tramite la shell, ma se proprio abbiamo delle necessità particolari possiamo in realtà inserire qualsiasi valore vogliamo per il campo `_id`: numeri, stringhe; tuttavia se non lo inseriamo noi allora MongoDB farà il lavoro in maniera automatica, mettendoci dentro un ObjectId.
Facciamo un altro inserimento nella nostra collezione:

```JavaScript
db.people.insert( { "name" : "Flaminia" , age : 25 , "profession" : "DBA" , "hobby" : "Volleyball" } )
```

e ripetiamo di nuovo la find `db.people.find()`; notiamo adesso i due documenti inseriti e vediamo come è cambiato il campo `_id`.
Giusto per anticipare alcune dritte sul comando `find`, possiamo dire che ci sono alcuni metodi che possiamo agganciare che potrebbero facilitarci la vita in caso di operazioni complesse da fare sulla shelll (o script che possiamo dover scrivere in caso di operazioni complesse); il primo comando è abbastanza semplice e ci permette di avere una rappresentazione visiva del risultato della find molto facile, si tratta di `.pretty()` e semplicemente fa l'indentazione dell'output del JSON, rendendoci appunto l'output più leggibile; il secondo è `.toArray()` e nel caso della find appunto ci permette di mettere il risultato dentro un array JavaScript e, in caso di necessità, salvare il tutto in una variabile che possiamo richiamare magari in seguito (`var array = db.people.find().toArray()`).
Altro comando molto importante che abbiamo forse visto anche in precedenza è il comando `.help()`, che funziona anche a gerarchie diverse (`db`, collezione), esempio `db.help()`.
Prima di vedere un po più al livello fisico i file generati all'interno del database precisiamo giusto una questione: nei comandi che abbiamo appena inserito abbiamo visto che gli oggetti JavaScript sono stati scritti nel formato chiave-valore mettendo tutte le chiavi e i campi valore in stringa tra doppi apici; questo è un comportamento dettato dagli standard per la rappresentazione JSON, tuttavia non sono strettamente necessari quando scriviamo oggetti JavaScript e infatti la shell di mongo accetta anche oggetti con chiavi non tra doppi apici.
Diciamo che è *buona norma* mettere i doppi apici alle chiavi, ma se per caso li dimenticate mentre inserite nuovi documenti nella shell o ci tenete particolarmente all'usura dei tasti della vostra tastiera allora non è un grande problema.

Per i più curiosi passiamo ora ad analizzare un ultimo aspetto prima di procedere oltre: chiudiamo la nostra shell mongo, o semplicemente apriamo un altro terminale, e navighiamo la cartella che abbiamo indicato durante l'installazione di MongoDB come base per il salvataggio dei file del database.
Eseguiamo quindi il comando `cd /data` (ipotizzando che sia sotto la cartella `/data`) e carichiamo i dettagli dei file che troviamo in questa cartella con il comando `ls -al`; notiamo qui come sono strutturati i file delle collezioni che abbiamo nel nostro database, questi di fatto sono file contenenti dati scritti in BSON, per cui nel caso il nostro database non fosse cifrato potremmo aprirli e leggere il contenuto dei vari documenti salvati.

Maggiori dettagli sul comando `insert` li trovate nella [sezione dedicata della documentazione ufficiale di MongoDB](https://docs.mongodb.com/manual/reference/method/db.collection.insert/).

Ok, ora che sappiamo come inserire documenti a mano nel nostro database tramite la shell mongo, cerchiamo di capire come tirarli fuori, utilizzando l'equivalente del comando `SELECT` nel linguaggio SQL.
L'operazione più comoda e immediata per effettuare una query su una collezione consiste nel comando `find`, tuttavia alle volte potremmo essere maggiormente interessati a tirar fuori un solo documento piuttosto che una lista e per questo scopo MongoDB mette a disposizione un metodo chiamato `findOne`.
Ricordiamo di tenere a mente che tutte le operazioni di CRUD effettuate in MongoDB sono sintatticamente presentate come metodi di collezioni e, ovviamente, `findOne` non fa differenza.
Ma torniamo sulla nostra shell per fare un paio di prove.
Il metodo `findOne` se eseguito senza argomenti (`db.people.findOne()`) ritorna il primo documento della collezione secondo *l'ordine naturale* di inserimento (breve parentesi a riguardo: per ordine naturale intendiamo l'ordine secondo il quale sono fisicamente salvati i file sul database e questo non indica necessariamente l'ordine di inserimento, per cui è più sicuro parlare di ordine casuale piuttosto che di un qualche ordinamento preciso).
Questa cosa potrebbe essere particolarmente utile se per esempio siamo giusto interessati allo schema che potrebbe avere una particolare collezione.
Ovviamente il metodo `findOne` può essere usato anche in maniera più intelligente e possiamo quindi dargli degli argomenti.
Il primo argomento che possiamo passare specifica il criterio di selezione della `findOne`, in modo tale che ritorni un documento che corrisponda al criterio; per dire, questo argomento equivale alla condizione di `WHERE` in una normale query SQL.
L'argomento utilizzado per il criterio di selezione è anch'esso un documento JavaScript (come l'output atteso), per cui un esempio di `findOne` potrebbe essere il seguente: `db.people.findOne( { "name" : "Marco" } )`; questo comando tornerà quindi il primo documento all'interno della collezione che avrà una corrispondenza con il documento specificato, ossia il primo documento che ha come chiave `name` e relativo valore la stringa `Marco`.
Questo fatto di specificare un documento per una condizione di selezione rappresenta una variazione significativa di come le cose verrebbero fatte in un normale database relazionale; invece di avere un linguaggio di interrogazione basato su stringhe (pensate al normale SQL), in MongoDB inviamo la codifica BSON dell'argomento (la condizione o criterio di selezione) alla `findOne` attraverso il *wire protocol* (wrapping in un qualche messaggio header) al database per essere eseguita, quello che succede quindi è che presentiamo al server il criterio di selezione sottoforma di documento strutturato invece di una sequenza di testo che necessariamente dovrà essere parsificata ed analizzata.
è importante tenere a mente questo concetto dell'invio di documenti; di fatto non è un concetto immediato ma è importante perchè in MongoDB non c'è alcun uso nativo di query basate sé stringhe.
Passiamo oltre e torniamo alla nostra `findOne`; abbiamo detto che possiamo usare il primo argomento come condizione di selezione e cercare nel database i documenti che abbiano un match con il documento specificato, cos'altro possiamo voler specificare a questo punto?
Possiamo avere ad esempio la necessità di visualizzare solamente alcuni campi del documento finale; questo ovviamente possiamo farlo e il secondo argomento del comando `findOne` ci permette esattamente di fare questa cosa.
Anch'esso, come per il primo argomento, è rappresentato tramite un documento, nel quale possiamo specificare quali campi vogliamo far visualizzare nel documento di output.
Per specificare quale campo vogliamo visualizzare basta inserire il nome del campo nel documento seguito dal valore booleano `true`, mentre al contrario se non vogliamo vedere un campo specifico basta inserire sempre il nome del campo ma questa volta seguito dal valore booleano `false`.
Il secondo parametro del metodo `findOne` equivale, se vogliamo, ai campi che possiamo specificare nella clausola `SELECT` nel linguaggio SQL, dove selezioniamo le colonne che vogliamo visualizzare in un risultato di output.
Il seguente è un valido esempio di quanto appena detto: `db.people.findOne( { "name" : "Marco" } , { "name" : true , "_id" : false } )`; eseguendo questo comando sulla shell vediamo che tornerà il primo documento che soddisfa la condizione di selezione `{ "name" : "Marco" }` e verrà visualizzato il solo campo `name`.
Ok, questo era un esempio abbastanza *stupido*, ma tuttavia ci permette di raccontare una particolarità di questa particolare feature: a meno che non venga specificato il contrario, come per l'esempio precedente, il campo `_id` di un documento risultante da una `findOne` viene sempre visualizzato, anche se non è stato esplicitamente richiesto (`{ "_id" : true }`), questo perchè viene mantenuta in un certo senso una vecchia funzionalità di MongoDB, il quale essendo originariamente pensato concettualmente come *object store*, si pensava alla necessità dé ottenere sempre indietro l'identificativo univoco di un oggetto salvato.

Maggiori dettagli sul comando `findOne` li trovate nella [sezione dedicata della documentazione ufficiale di MongoDB](https://docs.mongodb.com/manual/reference/method/db.collection.findOne/).

Facciamo ora un passo indietro, prima di effettuare il vero e proprio balzo in avanti sulle query, andando a vedere il comando `find`; questo metodo, come per buona parte degli altri metodi che vedremo più avanti, fa parte della collezione che lo invoca.
Il comportamento è simile al suo *fratello minore* `findOne`, con l'unica differenza che `find` ritorna l'intera lista di documenti della collezione che hanno un match con il criterio di selezione specificato, oppure ritorna tutti i documenti salvati nella collezione.
Facciamo un esempio ed interroghiamo la solita collezione `people`; eseguiamo quindi il comando `db.people.find()` per tornare l'intera lista di documenti salvati nella collezione.
Ce ne sono pochi ovviamente, solo quelli che abbiamo salvato durante la spiegazione precedente; inseriamone molti altri per rendere un po' più interessante la nostra spiegazione.
Come abbiamo visto più volte in precedenza, la shell mongo è di fatto un interprete JavaScript, eseguiamo quindi questo script al suo interno: `for(i = 0; i < 1000; i++) { names = [ "exam" , "essay" , "quiz" ]; for(j = 0; j < 3; j++){db.scores.insert( { "student" : i , "type" : names[j] , "score" : Math.round(Math.random() * 100) } );} }`, tale comando rappresenta in realtà due loop innestati, tramite i quali facciamo alcuni inserimenti nella collezione `scores`.
Eseguiamo ora di nuovo la find in questo modo: `db.scores.find()` e notiamo ora la linga lista di risultati.
Nella shell, quando una query ritorna un grande numero di documenti, questi vengono paginati (o meglio *batched*) e notiamo infatti che li ritroviamo a scaglioni di circa 20 documenti.
Per andare avanti e visualizzare altri documenti digitiamo il comando `it`, come anche suggerito dalla shell stessa; alla fine questa iterazione esaurisce tutti i documenti da visualizzare, un po' come se in un forum andiamo avanti con la paginazione fino a visualizzare l'ultimo thread.
Tutto questo accade perchè sul server viene mantenuto il cosiddetto *cursore* (o *cursor*), il quale tiene la query aperta per la shell e la mantiene tale per un periodo limitato (1é minuti), oppure fin quando il cursore non esaurisce tutti i documenti da far visualizzare.
Anche per la `find` vale la possibilità di abbellire i documenti risultati, per cui possiamo continuare a utillizzare il metodo `pretty()`.

Cominciamo ora un lungo passaggio per analizzare tutti i vari operatori che possiamo utilizzare nel criterio di selezione della `find` (e di conseguenza nella `findOne`).
Come abbiamo detto in precedenza, il metodo `find` prevede due argomenti opzionali: il primo è per settare i parametri di query, banalmente le condizioni di match per i documenti, mentre il secondo è per le proiezioni, ossia quali campi far visualizzare come risultato.
Abbiamo presentato un esempio abbastanza semplice in precedenza, ma come funziona se voglio specificare più clausole di selezione?
Beh, in realtà come forse avete già immaginato è più semplice di quanto si pensi, in quanto basta creare un documento da inserire nella query che abbia più campi di selezione.
Facciamo un esempio: riprendiamo la nostra collezione `scores` e cerchiamo il tema (*essay*) dello studente 19; la query avrà una forma simile a questa `db.scores.find( { "student" : 19 , "type" : "essay" } )`.
Abbiamo visto quindi come è molto semplice fare delle interrogazioni base che, facendo un parallelismo con il linguaggio SQL, mettono in *AND* alcuni valori all'interno dei documenti.

Maggiori dettagli sul comando `find` li trovate nella [sezione dedicata della documentazione ufficiale di MongoDB](https://docs.mongodb.com/manual/reference/method/db.collection.find/).

Nel nostro futuro utilizzo di MongoDB saremo fortunati se avremo a che fare con soli valori fissi, ma cosa succede se siamo alla ricerca di un range di valori?
A questo ci aiutano i primi due operatori della nostra lunga carrellata sono `$gt` e `$lt`, rispettivamente *greater than* e *lower than* (e volendo i meno restrittivi `$gte` e `$lte`).
Facciamo subito un esempio per chiarire la situazione: sempre nella nostra collezione `scores` siamo interessati a trovare tutti i temi che abbiano un voto maggiore di 95; questo ovviamente possiamo farlo in MongoDB specificando l'operatore corretto.
La sintassi è praticamente identica a quanto visto fin'ora, solo con alcuni accorgimenti legati all'utilizzo corretto degli operatori (e che infatti vedremo anche in seguito); in particolare per eseguire quanto descritto nell'esempio la nostra query avrà una forma simile a questa `db.scores.find( { "score" : { $gt : 95 } , "type" : "essay" } )`.
Vediamo infatti che in questo caso abbiamo inserito un documento innestato all'interno del documento della query, utilizzando l'operatore `$gt` e indicando il valore desiderato.
Eseguiamo il comando e vediamo che la shell ci ritorna la lista di tutti i temi con voto maggiore di 95.
Ovviamente siamo liberi di inserire più vincoli sullo stesso elemento, identificando ad esempio un range di valori ben circoscritto; riprendiamo il nostro esempio di query e ampliamolo per identificare tutti i temi che abbiano un voto compreso tra 95 (escluso) e 98 (incluso).
La query risultante sarà simile alla seguente `db.scores.find( { "score" : { $gt : 95 , $lte : 98 } , "type" : "essay" } )`.
Eseguiamo il comando e notiamo infatti che tutti i documenti di ritorno soddisfano questo nostro vincolo.
è importante a questo punto sottolineare che tutti e 2 i vincoli specificati (3 in realtà) devono essere soddisfatti contemporaneamente, in pratica come se mettessimo tutto in AND logico (facendo un parallelismo con SQL).

Passiamo ora a vedere un attimo nel dettaglio le ineguaglianze per i valori di testo (stringhe).
Abbiamo appena visto infatti che gli operatori di comparazione come `$gt` e `$lt` funzionano benissimo con valori numeri, ma cosa possiamo dire riguardo la comparazione di stringhe o semplicemente caratteri?
Beh, più meno le stesse considerazioni che abbiamo fatto in precedenza, in quanto tali operatori funzionano bene anche in questo caso.
Per rendere più chiaro questo concetto, ripeschiamo dal nostro database la collezione `people` e, già che ci siamo, aggiungiamo qualche altro documento per facilitare la comprensione (`db.people.insert( { "name" : "Alice" } ); db.people.insert( { "name" : "Bruna" } ); db.people.insert( { "name" : "Carlo" } ); db.people.insert( { "name" : "Davide" } ); db.people.insert( { "name" : "Elisa" } ); db.people.insert( { "name" : "Francesca" } );`); eseguiamo ora il comando `db.people.find( { "name" : { $lt : "D" } } )` che va a cercare tutti i documenti il cui campo `name` è **lessicograficamente** minore della lettera "D".
Ovviamente come per il caso numerico possiamo aggiungere altri vincoli, possiamo eseguire ad esempio il comando `db.people.find( { "name" : { $lt : "D" , $gt : "B" } } )`; in questo esempio notiamo che la stringa "Bruna" è lessicograficamente maggiore della lettera "B" semplicemente perchè di lunghezza maggiore ("Bruna": 5 caratteri, "B": un carattere)é
Come è possibile che MongoDB riesca a capire l'ordine lessicografico delle stringhe?
In realtà MongoDB non ha conoscenza di questa cosa e, quando richiesto, effettua un ordinamento dei documenti prima di ritornare il risultato della query al client (in questo caso la shell); in particolare viene eseguito l'ordinamento lessicografico dei bytes nella rappresentazione in UTF-8 delle stringhe.
Ci sarebbero tutte le varie considerazioni da fare per rappresentazioni POSIX e non, ma per quanto ci riguarda limitiamoci al fatto che le stringhe vengono di di fatto ordinate in modo *ascii*-betico.
Questo però ci apre verso un'ulteriore considerazione: se l'ordinamento viene fatto in base ASCII, è quindi possibile ordinare lettere e numeri contemporaneamente?
Vediamolo insieme con un esempio e allarghiamo la nostra collezione `people` con un ulteriore documento `db.people.insert( { "name" : 42 } )`; rieseguiamo di nuovo le query fatte poco fa e notiamo subito che il documento appena inserito non viene mostrato.
MongoDB infatti esegue comparazioni solo su tipi di dati uguali, evitando di spaziare su tipi diversi.
Volendoci spingere quindi oltre il vantaggio del polimorfismo di MongoDB, dobbiamo considerare che le comparazioni di questi ordinamenti lessicografici non oltrepassano i limiti dei tipi di dato (in altre parole: le stringhe non vengono ordinate insieme ai valori numerici).
Anticipiamo comunque che è possibile scrivere delle query di complessità leggermente maggiore affinchè sia possibile superare questo limite.

Maggiori dettagli sugli operatori `$gt`, `$gte`, `$lt` e `$lte` li trovate nelle relative sezioni della documentazione ufficiale di MongoDB ([greater than](https://docs.mongodb.com/manual/reference/operator/query/gt/#gt), [greater than or equal to](https://docs.mongodb.com/manual/reference/operator/query/gte/#gte), [lower than](https://docs.mongodb.com/manual/reference/operator/query/lt/#lt), [lower than or equal to](https://docs.mongodb.com/manual/reference/operator/query/lte/#lte)).

Analiziamo ora altri operatori utili per effettuare query con il metodo `find`, ma piuttosto che spiegarli nel dettaglio andiamo avanti per esempi.
Dunque, come abbiamo avuto modo di notare, nella nostra collezione `people` abbiamo un certo numero di documenti differenti tra di loro; alcuni hanno i campi `_id`, `name`, `age` e `profession`, mentre altri hanno solo i campi `_id` e `name`, ancora, alcuni documenti hanno il campo `name` formato da una stringa, mentre altri da un numero.
MongoDB ci permette di effettuare interrogazioni non solo sul valore dei vari campi, ma anche sulla struttura dei documenti; per esempio possiamo voler ricercare tutti i documenti che abbiano il campo `profession`, eseguendo quindi la seguente query `db.people.find( { profession : { $exists : true } } );`, dove appunto `profession` corrisponde a un documento embeddato contenente l'operatore `$exists` e il valore booleano `true`.
Al contrario posso richiedere solo i documenti in cui il campo `profession` non sia presente, cambiando il `true` in un `false`.
Posso inoltre cercare quei documenti in base al tipo di un determinato campo, ad esempio solo quei documenti dove il campo `name` è una stringa; il comando da eseguire in questo caso sarà il seguente: `db.people.find( { name : { $type : 2 } } )`.
Per il caso specifico dove viene richiesto il tipo del valore vedete che ho specificato il numero 2; fino alla versione precedente a quella attuale di MongoDB, l'operatore `$type` accettava solo un numero che corrispondeva a un tipo specifico di dato BSON.
Adesso fortunatamente possiamo contare su degli alias che ci rendono la vita un po' più facile, per cui aggiornando il nostro ultimo comando avremo una cosa del genere: `db.people.find( { name : { $type : "string" } } )`.
Facciamo ora un ulteriore avanzamento di livello e assumiamo che un operatore che lavora con un client per MongoDB possa di fatto necessitare di query più complesse, utilizzando magari dei patter per fare query su stringhe.
La buona notizia ovviamente è che MongoDB supporta questo tipo di operazioni utilizzando le espressioni regolari (in particolare fa affidamento sulla libreria [Perl Compatible Regular Expression](http://www.pcre.org/), o semplicemente **libpcre**); le espressioni regolari utilizzate avranno quindi una forma in stile Pearl.
Ovviamente questo corso non è sulle espressioni regolari, per cui non perderemo troppo tempo a spiegare come funzionano, ma ritengo sia comunque molto utile usare alcuni esempi per rendere più chiaro di cosa stiamo parlando.
Torniamo alla nostra collezione `people` e ricerchiamo tutti quei documenti il cui campo `name` contenga la lettera "a"; il comando finale avrà forse questa forma: `db.people.find( { name : { $regex : "a" } } )`.
Nel caso in cui la lettera "a" debba trovarsi alla fine della stringa del campo `name`, il comando da eseguire sarà il seguente: `db.people.find( { name : { $regex : "a$" } } )` (stessa stringa precedente ma seguita da un dollaro).
Ora, giusto per fare una breve parentesi, parleremo di ottimizzazione più avanti nel corso dei prossimi capitoli, tuttavia possiamo anticipare che le espressioni regolari tengono a non ottimizzare particolarmente l'esecuzione della query, proprio perchè il dover processare qualcosa che si allontana dalle disuguaglianze tende ad allungare il tempo di analisi dei varé campi; tuttavia c'è un caso in cui le espressioni regolari *scalano* bene, ed è quello in cui si usa un'espressione regolare per trovare tutti i documenti i cui campi cominciano per una determinata lettera.
In questo caso il comando sarà il seguente: `db.people.find( { name : { $regex : "^a" } } )`.
Questo particolare caso potrebbe essere tradotto in una range query in cui il valore della stringa sia compreso tra la lettera "a" (inclusa) e la successiva "b" (esclusa), in modo da velocizzare l'esecuzione e, guarda caso, è proprio quello che fa MongoDB "sotto il cofano".
Facendo quindi un recap, abbiamo che l'operatore `$exists` cerca un campo specifico all'interno del documento, `$type` controlla che il tipo del valore specificato sia di un tipo specifico (indicato nella query stessa) e `$regex` ci permette di cercare qualsiasi pattern di all'interno dei vari campi del documento.

Maggiori dettagli sugli operatori `$exists`, `$type` e `$regex` li trovate nelle relative sezioni della documentazione ufficiale di MongoDB ([exists](https://docs.mongodb.com/manual/reference/operator/query/exists/#exists), [type](https://docs.mongodb.com/manual/reference/operator/query/type/#type), [regex](https://docs.mongodb.com/manual/reference/operator/query/regex/#regex)).

Passiamo ora a due altri operatori.
Abbiamo visto dunque come fare comparazioni tra valori diversi e come controllare se un campo esiste e di quale tipo si tratta; vediamo ora come possiamo mettere insieme tutti gli operatori visti in precedenza in modo da formare una connessione logica tra loro.
Iniziamo con l'operatore `$or`.
Tutti immagino conosciamo bene qual è il funzionamento logico dell'operatore *OR*, il quale nel mondo MongoDB diventa *un documento che soddisfa un criterio oppure un altro criterio*.
Vediamo subito un esempio sulla shell per rendere immediatamente chiaro il concetto; dalla nostra collezione *people* prendiamo tutte le persone il cui nome termina con la lettera *e* oppure la cui età (campo `age`) sia specificata, il comando da eseguire nella nostra shell sarà il seguente: `db.people.find( { $or : [{ name : { $regex : "e$" } } , { age : { $exists : true } } ] } )`.
Una cosa che notiamo subito è che, al contrario degli altri operatori visti fino ad adesso, il `$or` è un operatore "prefisso" e funziona di fatto in questo modo: prende in considerazione un array di documenti, i quali di fatto rappresentano delle query *standalone* (ossia che potete far girare anche singolarmente) che vengono messe in relazione logica tra di loro, *OR* appunto.
I documenti che soddisfano questa query sono quindi tutti quelli che soddisfano almeno una delle "sotto-query" specificate all'interno dell'operatore `$or`, tutte le sotto-query sono trattate separatamente e l'operatore `$or` combina i risultati in modo da produrne l'unione.

Maggiori dettagli sull'operatore `$or` li trovate nella [sezione dedicata della documentazione ufficiale di MongoDB](https://docs.mongodb.com/manual/reference/operator/query/or/).

Per quelli di voi che non sono molto abituati ad avere a che fare con la scrittura di oggetti JSON, questa sintassi fatta di parentesi che si aprono e si chiudono formando documenti innestati potrebbe effettivamente dare un po' di disturbo.
Una tecnica che personalmente vi consiglio (e che uso regolarmente anche io) è quella di aprire e chiudere subito tutte le parentesi prima ancora di completarne il contenuto, in modo tale da avere già un'idea dell'a struttura del nostro documento, così da completarlo più facilmente.
Un altro aiuto ci viene dalla shell mongo, la quale se ci fate caso ci evidenzia le parentesi del documento corrente, in modo da farci capire se per caso abbiamo dimenticato una parentesi o ne abbiamo messa una di troppo.
Se per caso eseguite un comando con un numero errato di parentesi, la shell di mongo ci tornerà tre puntini, i quali stanno a significare che il comando non è completo (un po' come fa una normale shell Linux).
Per risolvere questa situazione basta premere due volte invio oppure semplicmente *ctrl-c* per sbloccare il comando e reinserirlo corretto.

Passiamo ora all'operatore logico `$and` che, come potete ben immaginare, implementa l'operazione logica di *AND*.
Il funzionamento, come anche la sintassi, è praticamente simile a quella vista per l'operatore `$or` e lo vediamo subito da un esempio: cerchiamo tutte le persone il cui nome sia *lessicograficamente maggiore* della lettera "C" e contenga all'interno la lettera "a", il comando da eseguire sarà il seguente: `db.people.find( { $and: [ { name : { $gt: "C" } } , { name : { $regex : "a" } } ] } )`.
C'è da dire che l'operatore `$and` è alquanto atipico e il suo utilizzo di fatto molto limitato, in quanto possiamo eseguire le stesse operazioni anche mettendo i parametri delle varie query che compongono l'and nello stesso documento ed effettuare una query normale; l'esempio che abbiamo appena mostrato in particolare è equivalente a questo: `db.people.find( { name : { $gt : "C" , $regex : "a" } } )`, sicuramente molto più snello e facile da comprendere.
MongoDB dunque effettua implicitamente l'operazione di *AND* anche nel caso di *query semplici*, ma allora perchè specificare anche un operatore dedicatoé
La risposta è che l'operatore `$and` si rende strettamente necessario quanto lo stesso campo o lo stesso operatore vengono specificati più volte nella stessa espressione.
Prendiamo un esempio forse un po' complesso ma che tuttavia rende abbastanza bene l'idea ([preso dalla documentazione ufficiale](https://docs.mongodb.com/manual/reference/operator/query/and/#and-queries-with-multiple-expressions-specifying-the-same-operator)): mettiamo caso di avere una collezione inventario (`inventory`) nella quale dobbiamo trovare tutti i prodotti il cui prezzo sia uguale a 0.99 oppure 1.99, oppure che sia in saldo (`sale : true`) e la quantità sia minore di 20 (`qty < 20`), l'espressione da usare sarà la seguente `db.inventory.find( { $and : [ { $or : [ { price : 0.99 } , { price : 1.99 } ] } , { $or : [ { sale : true } , { qty : { $lt : 20 } } ] } ] } )`.
In questo caso non sarebbe possibile in alcun modo "normalizzare" (passatemi il termine) la query in quanto utilizziamo l'operatore `$or` più di una volta.

Maggiori dettagli sull'operatore `$and` li trovate nella [sezione dedicata della documentazione ufficiale di MongoDB](https://docs.mongodb.com/manual/reference/operator/query/and/).

Per completezza riportiamo anche gli operatori `$not` e `$nor`.
L'operatore `$not` esegue l'operazione logica *NOT* su quanto specificato nel documento passato alla find e contenente i criteri di selezione, ritorna tutti i documenti di una collezione che non corrispondono a tali criteri; ad esempio il segente comando ritorna tutti i documenti della collezione inventario che non hanno il prezzo superiore a 1.99: `db.inventory.find( { price : { $not : { $gt : 1.99 } } } )`.
L'operatore `$nor` esegue l'operazione logica *NOR* su un array di documenti contenenti i vari criteri di selezione, ritorna tutti i documenti di una collezione che non soddisfano nessuno di tali criteri; ad esempio il seguente comando ritorna tutti i documenti della collezione inventario che non hanno il prezzo uguale a 1.99 e non sono in saldo: `db.inventory.find( { $nor : [ { price : 1.99 } , { sale : true } ] } )`.

Maggiori dettagli sugli operatori `$not` e `$nor` li trovate nelle relative sezioni della documentazione ufficiale di MongoDB ([not](https://docs.mongodb.com/manual/reference/operator/query/not/), [nor](https://docs.mongodb.com/manual/reference/operator/query/nor/)).

Inizialmente abbiamo parlato della possibilità di avere diversi tipi di dato all'interno di un documento salvato in MongoDB, ma cosa succede se un campo contiene un array di elementi, magari delle stringhe, e volessimo fare delle query su documenti contenente tali campi?
In realtà, grazie alla natura polimorfica di MongoDB, per query e strutture dato, diciamo, *semplici* è abbastanza semplice, in quanto possiamo mantenere la stessa struttura che avevamo per i primi esempi di query che abbiamo fatto.
Facciamo un esempio per rendere più chiare le cose e cominciamo con l'inserire due documenti nella collezione *accounts*: `db.accounts.insert( { nome : "Giorgio" , preferenze : [ "birra" , "torta" ] } )` e `db.accounts.insert( { nome : "Anna" , preferenze : [ "gelato" , "torta" ] } )`; notiamo che questi due documenti hanno entrambe il campo *preferenze* valorizzato con un array di stringhe.
Mettiamo ora di voler cercare in questa collezione tutte le persone che abbiano "torta" come preferenza, il comando da eseguire è semplicemente `db.accounts.find( { preferenze : "torta" } )`.
Questo funziona grazie alla natura polimorfica di MongoDB e in particolare l'esecuzione di questa query può essere intesa come '*cerca tutti i documenti nella collezione "accounts" che abbiano un campo chiamato "preferenze" il cui valore sia la stringa "torta", oppure un array al cui interno ci sia la stessa stringa "torta"*'.
Questa caratteristica di inserire array di dati in MongoDB in realtà è abbastanza nota e anzi risulta come valida alternativa durante la "denormalizzazione" dello schema del DB (argomento che vedremo più avanti quando parleremo di Schema Design).
è importante inoltre specificare che internamente non c'è alcuna ricorsione che viene attivata in questi casi, in quanto il flusso di esecuzione è più o meno simile a quanto descritto poco fa nella query (*cerca se il campo contiene quella specifica stringa, oppure se è un array certa all'interno di esso se quella specifica stringa esiste*).
Ovviamente questa caratteristica di effettuare ricerche in questo modo può anche essere abbinata ad altri operatori.

Continuiamo il nostro excursus sull'analisi degli array nelle query.
Abbiamo visto che è possibile usare il semplice criterio di match se siamo interessati a ricercare un solo elemento all'interno di un array, ma come possiamo fare nel caso in cui gli elementi da ricercare siano più di uno?
La soluzione ci viene dall'operatore `$all`, il quale ricerca tutti i documenti che abbiano un campo valorizzato come array, il quale contenga tutti gli elementi specificati tramite l'operatore `$all` stesso.
Facciamo un esempio per rendere più chiaro il concetto e inseriamo alcuni altri documenti nella nostra collezione *accounts*: `db.accounts.insert( { nome : "Luca" , preferenze : [ "formaggio" , "birra" ] } )` e `db.accounts.insert( { nome : "Marco" , preferenze : [ "gelato" , "torta" , "formaggio" ] } )`; ora nel caso volessimo estrarre dalla collezione tutti i documenti il cui campo `preferenze` contenga entrambe le stringhe "gelato" e "torta" (in qualsiasi ordine) il comando da eseguire sarà il seguente `db.accounts.find( { preferenze : { $all : [ "gelato" , "torta" ] } } )`.
Eseguiamo il comando ed ecco che, come previsto, abbiamo come risultato i documenti di "Marco" e "Anna", in quanto il primo preferisce il gelato, la torta e il formaggio, mentre la seconda preferisce proprio il gelato e la torta.
L'operatore `$all` quindi prende come parametro un array contenente un sottoinsieme (o proprio l'insieme completo) degli elementi presenti negli array dei documenti da ricercare.

Maggiori dettagli sull'operatore `$all` li trovate nella [sezione dedicata della documentazione ufficiale di MongoDB](https://docs.mongodb.com/manual/reference/operator/query/all/).

Altro operatore legato (ma non troppo) agli array è l'operatore `$in`, il quale prende una lista (array) di oggetti e ritorna solo quei documenti il cui campo specificato sia uguale ad almeno uno degli oggetti in lista.
Usando la stessa collezione di prima, mettiamo di voler cercare nella collezione *accounts* tutti i documenti il cui campo `nome` abbia i valori "Luca"" o "Marco".
Quello che facciamo in questo caso è di fatto un esame dentro l'array che abbiamo specificato e questa cosa ovviamente funziona anche per i campi che di fatto contengono altri array.
Riprendiamo l'esempio fatto giusto poco fa e cambiamolo in modo tale da cercare nelle preferenze le stringhe "birra" e "gelato"; il comando diventerà il seguente: `db.accounts.find( { preferenze : { $in : [ "birra" , "gelato" ] } } )`.
Eseguiamolo e di fatto vedremo che ci saranno documenti il cui campo preferenze contterrà la stringa "birra", le stringhe "birra" e "gelato", oppure solo la stringa "gelato".
Questo operatore risulta quindi abbastanza comodo proprio quando dobbiamo specificare dei possibili valori da ricercare sia su campi singoli che su campi con array.

Maggiori dettagli sull'operatore `$in` li trovate nella [sezione dedicata della documentazione ufficiale di MongoDB](https://docs.mongodb.com/manual/reference/operator/query/in/).

Ora che conosciamo bene come interfacciarci con i campi rappresentati tramite array, ci viene naturale chiederci: come possiamo fare per interagire allo stesso livello di flessibilità con i documenti innestati?
Come abbiamo visto fin dall'inizio infatti, MongoDB è basato sulla rappresentazione di informazioni in documenti, a loro volta *serializzati* come JSON; il formato JSON prevede ovviamente la possibilità di innestare documenti e questa cosa ce la ritroviamo chiaramente anche in MongoDB.
Ora che sappiamo che i documenti salvati in MongoDB possono contenere al loro interno altri documenti innestati, capiamo come fare per effettuare delle query che possano andare a "lavorare" dentro tali documenti.
Proviamo innanzitutto a creare un documento che ne innesti un altro; eseguiamo questo comando: `db.users.insert( { name : "Laura" , email : { work : "anna@consoft.it" , personal : "anna@gmail.com" } } )`.
Abbiamo quindi inserito nella collezione *users* un documento che ha come campi il nome della persone e le sue email; `email` è di fatto rappresentato da un documento innestato vero e proprio, con i campi `work` e `personal` (a rappresentare un indirizzo email lavorativo e uno personale).
Un primo possibile esempio di query che ci può venire in mente è quello di ricercare dei documenti tramite un preciso criterio di match, un po' come abbiamo fatto fin'ora (escludendo le espressioni regolari), effettuando quindi la seguente ricerca `db.users.find( { email : { work : "anna@consoft.it", personal : "anna@gmail.com" } } )`.
Tale comando ovviamente funzionerà senza problemi e ritornerà esattamente lo stesso documento inserito poco fa.
A questo punto però cominciano a sorgere dei problemi importanti: cosa succede se io sviluppatore non conosco esattamente la struttura del documento embeddato?
Come faccio quindi a capire esattamente come strutturare il criterio di selezione affinchè la query ritorni effettivamente un risultato valido?
Eh si perchè già per esempio se avessimo invertito l'ordine di `personal` e `work` la nostra ricerca non avrebbe sortito lo stesso effetto (l'ordine con il quale viene specificato ié documento embeddato deve essere preservato), stesso risultato se avessimo inserito un solo campo per effettuare la ricerca (`work` ad esempio).
Giusto per dare un po' di note sul funzionamento interno: questo succede perchè quando specifichiamo un criterio di selezione in una query, se dopo i `:` non inseriamo un operatore (`$`)é tutto cio' che segue viene analizzato e comparato così com'è *byte-by-byte*, per cui se la rappresentazione di un documento è fisicamente diversa, anche se concettualmente simile, la query non produrrà lo stesso risultato.
Tornando quindi alla nostra ipotesi iniziale, se noi sviluppatori non conoscessimo con precisione il documento embeddato ma sapessimo con certezza di avere un campo `work` sul quale poter effettuare la nostra query, come possiamo fare?
Anche in questo caso MongoDB ci mette a disposizione un utile strumento per questo genere di situazioni e quelli di voi che sono abituati a trattare con oggetti JSON potrebbero già aver capito di cosa si tratta; parliamo infatti della notazione con il punto, o *dot notation*.
Riprendiamo il nostro esempio precedente e mettiamo adesso di voler ricercare lo stesso documento di prima ma solo tramite l'indirizzo email di lavoro; il comando da eseguire sarà il seguente: `db.users.find( { "email.work" : "anna@consoft.it" } )`.
Per eseguire questa query il motore di MongoDB cercherà dunque nella collezione *users* tutti quei documenti che abbiano il campo `email`, andando tramite ricorsione a cercare dentro il documento embeddato se esiste un campo nominato `work` e, concludendo, se questo è effettivamente identico a quanto specificato nel criterio di selezione; se abbiamo una riscontro positivo allora il documento viene di fatto riportato come risultato, indipendentemente dalla struttura del documento embeddato e dall'ordine col quale viene salvato sul database.
La *dot notation* ci permette quindi di raggiungere elementi innestati dentro documenti embeddati nell'originale, alla ricerca di un campo o di un valore specifico senza la reale necessità di conoscere esattamente l'intera struttura del suddetto documento embeddato e del relativo contesto (altri campi e ordinamento degli stessi).
Aggiungiamo per completezza che tutto quanto detto fino ad ora sui vari operatori che abbiamo visto ovviamente vale anche qui e ovviamente possiamo scendere di tanti livelli di *nesting* quanti sono effettivamente quelli presenti nei documenti salvati.

Passiamo ora a un altro argomento e cerchiamo di capire meglio cosa succede quando eseguiamo una query e ne visualizziamo i risultati.
Parliamo infatti di **cursori** (*cursor*); qualcuno di voi forse avrà già avuto modo di interagire con questa funzionalità in altri ambienti magari relazionali e, di fatto, il concetto che applica MongoDB sui cursori è più o meno lo stesso di altri database.
Presentiamone ora i concetti generali di funzionamento: abbiamo quindi la nostra generica architettura con un'applicazione client e il server MongoD (`mongod`); a un certo punto ci sarà una richiesta query che verrà inviata dal client al server, il quale risponderà con un set di risultati, diciamo una prima *batch*, insieme a un *cursor ID*.
Questo sarà appunto il nostro primo set di risultati, diciamo circa 1000 documenti o qualcosa del genere.
Il risultato complessivo della nostra query tuttavia presenta un numero di documenti molto maggiore, per cui richiediamo ulteriori risultati con un generico comando `getMore()`; `mongod` ci risponde inviandoci una nuova batch di risultati e così via fino a quando non siamo soddisfatti del risultato della nostra query o non esauriamo il cursore e quindi il set di documenti ritornati dalla nostra query.
Vediamo quindi che il comportamento che MongoDB impone alle query tramite i cursori è si simile ad altri database ma si distacca in parte per questo comportamento particolarmente *lazy* se vogliamo, pigro.
Pensiamo al caso in cui avessimo una collezione da un miliardo di documenti e volessimo fare una query su tale collezione; il server di MongoDB di sicuro non si mette a creare un set di risultati per l'intera collezione, piuttosto ci ritorna di volta in volta un batch di dimensioni ragionevoli, sul quale possiamo fare di fatto tutte le operazioni che vogliamo, lasciando a noi la responsabilità di richiedere ulteriori documenti in caso di necessità.
Tutto questo ovviamente avviene internamente tra il client (o sarebbe meglio dire il driver MongoDB sul client) e il server MongoDB stesso; noi utenti di fatto non dobbiamo fare nulla in maniera esplicita, se non mandare in esecuzione i comandi che richiediamo, demandando magari a un cursore la necessità o meno di fornirci ulteriori risultati per le nostre query.
Passiamo ora un attimo sulla shell e valutiamo come tutto questo viene gestito.
Sulle collezioni create fin'ora non abbiamo molta visibilità di questa cosa in quanto il set di documenti è abbastanza limitato; creiamoci quindi una collezione con 20k documenti in questo modo: `for(var i = 0; i < 20000; i++) {db.test.insert( { x : i , y : "hello" } ); }` e controlliamo che effettivamente l'inserimento è stato effettuato andando a fare una conta dei documenti in questo modo: `db.test.count()`.
Eseguiamo il comando `db.test.find()` per eseguire una query sulla collezione appena popolata e, come possiamo ben immaginare, la shell mongo ci ritorna soltanto un sub-set di documenti, 20 per la precisione, chiedendoci di digitare `it` per avere il set successivo; digitiamo appunto `it` (che se ricordate equivale al nostro `getMore` specificato in precedenza) per ottenere un altro set, e così via.
è importante specificare tuttavia che il server non manda necessariamente tutti i documenti al nostro client subito, anzi, nel caso di risultati molto "grandi" è molto probabile che mandi i documenti, diciamo, 1000 alla volta, sta poi alla shell creare dei sub-set ancora minori per facilitarne la fruizione tramite linea di comando; il tutto ovviamente sfruttando proprio i cursori.
Questo discorso ovviamente vale in maniera abbastanza simile anche per altri driver, dove però la sintassi è leggermente differente.
Ma torniamo di fatto un attimo sui cursori veri e propri e come possiamo interagire in maniera diretta con essi.
Come ormai sappiamo benissimo, la shell mongo rappresenta un interprete JavaScript, per cui possiamo magari dichiarare variabili e scrivere brevi script da far eseguire; sfruttiamo questa cosa per salvarci su una nostra variabile interna il cursore che ritorna da una normale query `db.people.find()` e scriviamo appunto `var cur = db.people.find(); null` (mettiamo il `null` per forzare la shell a non visualizzare l'output del comando find, in realtà qualsiasi stringa va bene, purchè blocchi l'output del comando precedente).
Bene, a questo punto abbiamo la nostra variabile `cur` che contiene il risultato della *find*, ma attenzione, come abbiamo visto in precedenza, il risultato della *find* consiste anche in un `cursor ID`, ed è di fatto quello che viene salvato nella nostra variabile.
Questo cursore può eseguire diversi metodi e, per completezza, vi consiglio già da adesso di eseguire il comando `db.people.find().help()`, che esegue l'help sul risultato della *find*, il cursore appunto, per visualizzare tutti i metodi disponibili.
Possiamo ad esempio vedere se il cursore ha il *prossimo documento* a disposizione eseguendo il comando `cur.hasNext()`, che ritorna `true` o `false` a seconda che ci sia o meno disponibile un documento nel cursore; in caso positivo possiamo di fatto accedere a tale documento eseguendo questo comando `cur.next()` che ritorna proprio la rappresentazione JSON del documento disponibile.
Già con questi due soli metodi possiamo effettivamente tirare fuori l'intero set di documenti ottenibili da un cursore semplicemente scrivendo uno script che itera sul cursore stesso, in questo modo: `while (cur.hasNext()) printjson(cur.next())` (il metodo `printjson` è specifico della shell mongo e, come possiamo ben immaginare, crea una rappresentazione JSON del documento disponibile).
Eseguiamo il comando e vediamo che di fatto ci restituisce tutti i documenti ottenibili dal cursore.
Notiamo comunque che questo comportamento è pressochè identico a come si comporta un normale cursore quando viene interrogato, con l'unica differenza che in quel caso ci si ferma di 20 documenti alla volta.
Ovviamente sarà molto difficile che vi mettiate a fare script di interrogazione sulla shell mongo utilizzando tali comandi, ma pensiamo ad esempio ad ambienti tipo Python, o meglio ancora Java, dove di fatto questo genere di comandi vengono utilizzati moltissimo.
Analizziamo ancora altri metodi che possiamo applicare al cursore; il metodo `limit` per esempio è uno di quelli e, come possiamo immaginare, serve ad imporre un limite al numero di elementi tornati dal server, in particolare tale metodo forza il server a tornare solo un numero di documenti al più pari al numero specificato nel metodo limit; reimpostiamo il cursore (riassegnando la variabile cur con il risultato della find) e specifichiamo un limite in questo modo: `cur.limit(5); null;` (specifichiamo nuovamente la stringa `null` per evitare di far girare il cursore).
Quello che abbiamo fatto quindi è stato modificare il cursore affinchè selezioni soltanto 5 documenti ma attenzione, noi ancora non abbiamo fatto partire ancora nessuna reale interrogazione al database.
Quello che succede infatti è che fino a quando non richiedo il primo documento al cursore o semplicemente controllo che ci siano effettivamente dei documenti come risultato della mia query, nessuna trasmissione viene effettuata al server e, di conseguenza, nessuna query viene eseguita.
Solo quando richiedo al cursore di farmi visualizzare il risultato della query viene di fatto inviata ed eseguita la richiesta sul server MongoDB.
Passiamo ora a un altro metodo modificatore associato a un cursore, parliamo quindi del metodo `sort`; tale metodo effettua un ordinamento sul risultato del cursore in base al campo passato come argomento al metodo *sort* stesso.
La sintassi è abbastanza semplice e possiamo subito vederne un esempio: mettiamo di voler tornare dal nostro cursore la lista di documenti ordinata con *ordine inverso* sul campo `name`, il comando sarà quindi il seguente `cur.sort( { name : -1 } ); null` (come per il modificatore precedente, anche questo ritorna un cursore come risultato, per cui dobbiamo ricordarci di inserire il nostro *comando magico* `null`, oppure creare una catena di modificatori). 
*Sort* prende in input un documento JSON che abbia uno o più campi la cui chiave è il nome del campo del documento e il valore `+1` o `-1` rispettivamente per l'ordinamento normale e inverso sul campo specificato.
Reiterando sul cursore come fatto in precedenza, vedremo ora che i documenti ritornati tramite il ciclo *while* saranno proprio ordinati inversamente in base al valore contenuto nel campo `name`; ricordiamoci sempre che quando effettuiamo ordinamenti su stringhe quello che otterremo è sempre un *ordinamento lessicografico* in base alla rappresentazione delle stringhe stesse codificate in UTF-8 (in parole povere un ordinamento [ASCII](http://www.asciitable.com/)).
Importante dire a questo punto che possiamo combinare nello stesso comando più modificatori sullo stesso cursore, andando a specificare una vera e propria catena di comandi (abbastanza tipica se vogliamo negli ambienti Java e JavaScript); ma facciamo subito un esempio mettendo insieme quando visto fin'ora: cominciamo sempre ripristinando il nostro cursore, in modo tale da ristabilire l'iterazione e mettiamo di voler mantenere l'ordine inverso dei documenti in base al loro campo `name` e, in più, limitare il risultato a 3 soli documenti.
Il comando finale sarà, come possiamo ben immaginare, il seguente: `cur.sort( { name : -1 } ).limit(3); null`.
Iteriamo ora attraverso il cursore tramite il ciclo while e vedremo infatti che il risultato sarà composto da soli 3 documenti, ordinati in base al loro campo `name`.
Arrivati a questo punto è estremamente importante capire che i modificatori *modificano* le informazioni che vengono trasferite al server, prima ancora quindi che le query vengano di fatto eseguite dal motore di MongoDB.
Non posso infatti applicare nuovi modificatori a un cursore che è già stato "interrogato", in quanto tutti i dettagli aggiunti dai vari modificatori applicati a un cursore devono necessariamente essere processati dal motore di MongoDB; in altre parole, i modificatori non lavorano lato client ma sul server.
Ultimo modificatore che possiamo vedere adesso è quello per saltare un certo numero di documenti, si tratta infatti del metodo `skip`.
Esattamente come per *sort* e *limit*, *skip* va a modificare un cursore, il quale una volta interrogato ritorna quanto specificato tramite i modificatori stessi.
Facciamo un ultimo esempio che raccoglie tutti e tre i modificatori visti in modo da rendere chiaro il concetto: mettiamo quindi di volere la solita lista di documenti ordinati in modo lessicograficamente inverso, limitando a 3 il numero dei documenti risultati e saltando i primi 2; il comando sarà quindi il seguente: `cur.sort( { name : -1 } ).limit(3).skip(2)`.
I suddetti modificatori potrebbero volendo essere paragonati alle direttive SQL che tutti conosciamo `ORDER BY`, `LIMIT` e `SKIP`.
Importante specificare a questo punto anche che le specifiche imposte dai modificatori vengono applicate così come sono specificate al cursore, per cui nel nostro esempio avremo che prima viene eseguita la *sort*, poi *limit* e in fine *skip*, tutte ripetiamo sempre lato server e mai sul client, anche se di fatto può sembrare il contrario (stiamo modificando le istruzioni che vengono passate al database).
Ultima nota a margine: l'operazione di skip, in caso di una mole di dati significativa (partiamo già da ~5000 documenti) risulta di fatto poco efficiente, per cui se possibile andrebbe evitata in modo da non influire negativamente sulle prestazioni del server MongoDB.

Maggiori dettagli sulle iterazioni tramite cursori li trovate nella [sezione dedicata della documentazione ufficiale di MongoDB](https://docs.mongodb.com/manual/tutorial/iterate-a-cursor/).

Bene, ora che conosciamo i cursori in MongoDB, e i relativi modificatori, sappiamo dunque come interagire tramite query con il server.
Passiamo ora ad analizzare molto velocemente un metodo che, in realtà, abbiamo già visto di sfuggita negli esempi precedenti.
Mettiamo caso di voler fare una query sulla shell (o comunque tramite qualsiasi altra applicazione che includa il driver MongoDB) e di essere interessati più che altro alla quantità di documenti tornati dalla query stessa; MongoDB ci mette ovviamente a disposizione un metodo apposito per effettuare tale conteggio e, come avrete già avuto modo di capire, tale metodo è il `count()`.
Facciamo subito un esempio e riprendiamo una nostra vecchia collezione: mettiamo quindi di voler conoscere quanti documenti ci sono nella collezione delle valutazioni `scores` che siano degli esami (`exam`), il comando sarà semplicemente il seguente: `db.scores.count( { type : "exam" } )`.
Notiamo subito che il metodo *count* fa riferimento alla collezione, ed è di fatto un metodo della collezione stessa e che prende come input un documento contenente i criteri di selezione che vogliamo applicare.
Il funzionamento infatti è molto simile alla *find* che conosciamo bene, con l'unica differenza che invece di ritornare i documenti torniamo la quantità dei documenti stessi.
Piccola nota prima di passare oltre: `count` è in realtà anche un modificatore e varia ovviamente la sua natura in base a cosa usiamo per effettuarne la chiamata; il comportamento tuttavia è assolutamente analogo e in fatti nel caso del modificatore viene calcolato il numero di documenti che verranno poi referenziati dal cursore.

Maggiori dettagli sul metodo `count` li trovate nella [sezione dedicata della documentazione ufficiale di MongoDB](https://docs.mongodb.com/manual/reference/method/db.collection.count/).

Ok, arrivati a questo punto direi che abbiamo una buona panoramica su come interrogare il database e le collezioni contenute, per cui possiamo avanzare alla prossima discussione e cerchiamo di capire come fare per aggiornare o modificare un documento in una collezione.
Ovviamente come abbiamo un metodo per interrogare una collezione (metodo `find`) abbiamo anche un metodo per effettuarne la modifica, che è appunto il comando `update`; come la `find`, anche `update` è un comando relativo alle collezioni.
Il comando ha questa sintassi: `db.<nome_collezione>.update(<condizione_where> , <documento>)`, dove `condizione_where` contiene il criterio di selezione che usiamo per identificare il documento che vogliamo modificare, mentre `documento` contiene appunto il documento JSON con le modifiche da applicare.
Il comando `update` ha in realtà altre due opzioni non obligatorie che stanno per *upsert* e *multi*, dove il primo specifica se l'update ha comportamento di upsert (inserisci se mancante) e il secondo specifica se l'update deve andare a lavorare su più documenti all'interno della collezione.
Andiamo ora nel dettaglio del funzionamento di questo comando e specifichiamo subito un concetto importante.
L'update di un documento all'interno di una collezione MongoDB può in realtà avvenire secondo due comportamenti: il primo si chiama *full document update* ed è quello che vedremo subito e consiste nella sostituzione completa del documento salvato nella collezione, mentre il secondo si chiama *partial document update* e consiste nel modificare solo alcune parti del documento salvato.
La prima opzione è sicuramente quella più semplice per cui partiremo proprio da quella, tuttavia è importante capire che la *full document update* sostituisce completamente il documento salvato e identificato dalla condizione di selezione, ad esclusione della chiave primaria; la chiave `_id` è infatti immutabile e anzi, se prevediamo una collezione di documenti in cui ci sia un campo univoco immutabile nel tempo, ha senso porlo come chiave primaria (ad esempio il codice fiscale in una collezione di persone).
Questo comportamento, se vogliamo estremo e in un certo senso abbastanza pericoloso, deriva dal fatto che MongoDB è stato pensato inizialmente come *object store*, dove di fatto la questione del replacement completo è abbastanza normale se vogliamo.
Per risolvere la questione ci sono degli operatori particolari che vedremo a breve e che di fatto portano il comando `update` ad agire come *partial document update*.
Passiamo ora a fare qualche esempio sulla shell mondo, così da fissare il concetto della modifica su MongoDB.
Riprendiamo una collezione qualsiasi di test, creata eseguendo questo comando sulla shell: `for(var i = 0; i < 10; i++) {db.test.insert( { x : i , y : "hello" } ); }`, eseguiamo una `find` per vedere la collezione salvata e decidiamo di modificare ad esempio l'elemento dove `{ x : 1 }`; il comando da eseguire sarà il seguente: `db.test.update( { x : 1 } , { x : 1 , y : "hello consoft" , z : "ciaociao" } )`.
Rieseguendo nuovamente la `find` vediamo infatti che il documento che abbiamo inviato tramite l'update è di fatto lo stesso documento che ci viene fuori dalla find, per cui l'operazione ha avuto successo.
Sfruttando la shell volendo abbiamo anche un altra opzione che possiamo sfruttare; ricordiamo infatti che la shell mongo non è nient'altro che un interprete JavaScript, per cui quello che possiamo fare è eseguire la modifica passando direttamente un oggetto salvato in una variabile come documento da aggiornare.
Per farlo eseguiamo prima una find in questo modo: `var myObj = db.test.find( { x : 1 } )`, ora abbiamo il documento salvato nella variabile `myObj`, possiamo ora modificare l'oggetto stesso come faremmo tramite un normale oggetto JavaScript, possiamo ad esempio aggiungere un altro campo `myObj.count = 10`.
Ora che l'oggetto è modificato possiamo passarlo al comando `update` nel seguente modo: `db.test.update( { x : 1 } , myObj )`; se rieseguiamo la find vediamo lo stesso oggetto con le modifiche appena apportate.
Ora che abbiamo capito a grandi linee il comportamento generale della update permettetemi di fare giusto una breve parentesi sull'ordinamento delle collezioni e la rappresentazione BSON dei documenti; come forse avrete notato effettuando le ultime due find, è possibile che l'ordine della collezione stessa sia cambiato dopo la modifica del documento.
Questo dipende dal fatto che quando un documento cresce in termini di spazio (come nel nostro caso che abbiamo aggiunto un nuovo campo), la rappresentazione BSON del documento cambia di conseguenza e, se ricordiamo la tipologia di rappresentazione di un BSON vista nel primo capitolo, possiamo capire quindi che lo slot di rappresentazione dedicato non riesce più a contenere il documento con le nuove modifiche, quindi quello che succede è che il motore di MongoDB ha semplicemente deciso di cambiare lo slot, spostando i byte del documento modificato in un altra sezione dedicata alla collezione, grande abbastanza da contenere le modifiche inserite.
Ovviamente se proprio ci tenete a forzare l'ordinamento di inserimento all'interno di una collezione potete farlo lo stesso tramite quelle che sono le *Capped Collections* (queste collezioni non verranno trattate in maniera approfondita in questo corso, per cui se siete particolarmente interessati potete trovare maggiori dettagli nella [sezione dedicata del manuale ufficiale di MongoDB](https://docs.mongodb.com/manual/core/capped-collections/)).

Maggiori dettagli sul metodo `update` li trovate nella [sezione dedicata della documentazione ufficiale di MongoDB](https://docs.mongodb.com/manual/reference/method/db.collection.update/).

@@TODO: aggiungere i quiz su update nelle slide

A titolo di completezza possiamo presentare un ulteriore comando per effettuare una *full document update*; tale comando è il comando `save()`.
Una nota importante è rappresentata dal fatto che tale comando è un artefatto esclusivamente legato alla shell, in quanto non esiste una corrispettiva funzione di MongoDB; si tratta quindi di uno *shell helper*.
Facciamo un esempio per capirne il funzionamento: si tratta sempre di un metodo associato a una collezione, per cui dobbiamo comunque far riferimento a essa; mettiamo caso di voler modificare un campo di `myObj` e lo facciamo in questo modo: `myObj.y = 400`, ora semplicemente eseguiamo questo comando `db.test.save(myObj)`.
Eseguiamo ora una `find` e vediamo che l'oggetto salvato su database di fatto contiene la modifica appena salvata.
Per dimostrare che la `save` è un artefatto della shell possiamo eseguire il comando `save` senza le parentesi, in questo modo: `db.test.save` e vediamo che il codice JavaScript che ne viene fuori di fatto si articola in diversi casi, dove in uno viene eseguita una `insert` e nell'altro una `update`.

Maggiori dettagli sul metodo `save` li trovate nella [sezione dedicata della documentazione ufficiale di MongoDB](https://docs.mongodb.com/manual/reference/method/db.collection.save/).

@@TODO: aggiungere i quiz su save nelle slide

Bene, ora che abbiamo visto come fare per eseguire gli update completi, vediamo come fare per quelli parziali.
Giusto per inquadrare meglio il contesto, pensiamo ai *partial document update* come alla seguente stringa SQL: `UPDATE orders SET avail = 0 WHERE order_id = 10`.
Di fatto in MongoDB tali update sono forse più importanti di quelli completi, proprio per via della flessibilità e complessità che possono raggiungere i documenti, magari quelli di grandi dimensioni e con molteplici livelli di innestamento.
Proprio in questi casi pensiamo a quanto può risultare controproducente dover ogni volta estrarre mega documenti, trattarli e salvarli nuovamente nella collezione.
MongoDB ci mette a disposizione gli strumenti adeguati per effettuare queste modifiche parziali senza la necessità di dover estrarre prima completamente il documento da modificare; tali operazioni sono possibili tramite degli operatori dedicati e ben definiti che trovate nella [sezione dedicata del manuale di MongoDB](https://docs.mongodb.com/manual/reference/operator/update/) e che ora andremo ad analizzare.

Cominciamo con l'operatore forse più interessante per effettuare una modifica su uno specifico campo del documento, l'operatore `$set`.
Tale operatore è in realtà abbastanza semplice da utilizzare e la sintassi è la seguente: `db.<nome_collezione>.update( <condizione_where> , { $set : { <nome_campo> : <nuovo_valore> } } )`.
Come vediamo la prima parte dell'update restra pressochè identica, mentre nella seconda parte continuiamo a specificare un documento JavaScript, ma stavolta abbiamo l'operatore `$set` che prende come parametro un altro documento con all'interno il campo da modificare (`nome_campo`) e il nuovo valore da associare a tale campo (`nuovo_valore`).
Ma cosa succede se nella `set` specifichiamo un campo che non esiste?
La risposta è semplice: viene creato.
Sia che vogliamo modificare un campo già esistente nel documento, sia che vogliamo crearne uno nuovo, l'operatore `$set` risulta essere la scelta giusta.
Eseguiamo un esempio per renderne più chiaro il funzionamento; riprendiamo la nostra collezione di persone (`people`) e mettiamo caso di voler aggiornare il documento su Alice inserendo l'età, per fare questa operazione il comando update avrà la seguente forma: `db.people.update( { name : "Alice" } , { $set : { age : 25 } } )`.
Eseguiamo ora una find e vediamo che il documento su Alice contiene ora la sua età.
Piccola nota a margine: nell'update non sono stati inseriti i doppi apici (`"`) nei documenti del comando update; questa è una scelta puramente stilistica per alleggerire la lettura del codice, e JavaScript ce lo permette; tuttavia è importante ricordare che secondo lo standard JSON sia i campi che i valori di tali campi devono essere contenuti in doppi apici (ad eccezione di numeri, booleani e altri oggetti).
Giusto per presentare un altro operatore che bene si adatta a questo esempio, e che comunque potete trovare nella lista di operatori presentata poco fa, abbiamo l'operatore `$inc`, il quale semplicemente effettua un operazione di incremento rispetto a un campo specifico e di uno step equivalente al numero specificato tramite l'operatore stesso.
Anche in questo caso, se il campo non esiste nel documento da modificare, questo viene creato e settato uguale al valore indicato come step di incremento.
Vediamo ora un esempio sfruttando la stessa collezione e mettiamo di voler modificare l'età di Alice aggiungendo un anno; il comando da eseguire sarà il seguente: `db.people.update( { name : "Alice" } , { $inc : { age : 1 } } )`.
Per decrementare il valore di un campo è sufficiente specificare un valore negativo.

Maggiori dettagli sull'operatore `$set` e l'operatore `$inc` li trovate nelle sezioni dedicate della documentazione ufficiale di MongoDB ([set](https://docs.mongodb.com/manual/reference/operator/update/set/#up._S_set), [inc](https://docs.mongodb.com/manual/reference/operator/update/inc/#up._S_inc)).

@@TODO: aggiungere i quiz su $set nelle slide

Occasionalmente possiamo voler rimuovere determinati campi da un documento, questo può succedere ad esempio quando vogliamo effettuare operazioni di schema design o semplicemente per adattare la struttura di determinati documenti a nuove esigenze.
Per effettuare tale trasformazione di un documento possiamo usare l'operatore `$unset`, che prende in input un documento contenente il nome del campo da rimuovere, seguito dal numero 1 (in realtà quale valore specificare è assolutamente irrilevante al fine dell'operazione, specifichiamo 1 per mantenere una linearità nei documenti).
Facciamo un esempio e mettiamo di voler rimuovere il campo `age` dal documento di Alice, inserito nell'esempio precedente; per fare cio' il comando da eseguire sarà il seguente `db.people.update( { name : "Alice" } , { $unset : { age : 1 } } )`.

Maggiori dettagli sull'operatore `$unset` li trovate nella [sezione dedicata della documentazione ufficiale di MongoDB](https://docs.mongodb.com/manual/reference/operator/update/unset/).

@@TODO: aggiungere i quiz su $unset nelle slide

Bene, fin'ora abbiamo visto come aggiornare i normali campi all'interno di un documento, ma come facciamo ad aggiornare quei campi che contengono degli array?
Anche in questo caso MongoDB ci viene incontro fornendoci tutta una serie di operatori esclusivamente dedicati alla gestione e modifica degli array, e sono `$set` (valido anche per gli array), `$push`, `$pop`, `$pushAll`, `$pull`, `$pullAll` e `$addtoSet`.
Cominciamo con il primo, `$set`, e creiamoci una collezione di supporto utile per i vari esempi: `db.arrays.insert( { _id : 0 , a : [ 1, 2, 3, 4 ] } )`.
Come abbiamo visto in precedenza, l'operatore *set* serve appunto ad effettuare modifiche localizzate in un *update* parziale e questa cosa vale anche per gli array, dove bisogna specificare tuttavia una sintassi un po' particolare, in quando dobbiamo andare a localizzare l'elemento all'interno di un array; il nome del campo all'interno del documento contenente le modifiche deve essere appunto quello dell'array da modificare, seguito da un punto e dall'indice dell'elemento all'interno dello stesso array.
Il seguente esempio chiarirà sicuramente le idee: mettiamo di voler cambiare il terzo elemento dell'array `a`; il comando da eseguire sarà il seguente: `db.arrays.update( { _id : 0 } , { $set : { a.2 : 5 } } )`.

L'operatore `$set` serve quindi a modificare uno specifico elemento all'interno di un array, ma come faccio ad inserire un elemento in fondo all'array (lato destro)?
L'operatore `$push` ha esattamente questo scopo; prende in input un documento contenente il nome del campo dell'array e il nuovo elemento effettivo da inserire.
Mettiamo di vole inserire il numero 6 dentro l'array `a`, il comando da eseguire sarà il seguente: `db.arrays.update( { _id : 0 } , { $push : { a : 6 } } )`. 

Per rimuovere invece un elemento agli estremi di un array è possibile sfruttare l'operatore `$pop`, che va ad eliminare il documento più a destra o quello più a sinistra in base al parametro inserito all'interno del documento passato all'operatore; in particolare viene cancellato l'elemento a destra se il valore specificato è 1, mentre viene cancellato quello a sinistra se il valore è -1.
Se ad esempio volessimo rimuovere il valore inserito nell'esempio precedente, il comando sarà il seguente: `db.arrays.update( { _id : 0 } , { $pop : { a : 1 } } )`, mentre se volessimo rimuovere l'elemento più a sinistra, allora il comando sarà il seguente: `db.arrays.update( { _id : 0 } , { $pop : { a : -1 } } )`.

Nel caso in cui dovessimo invece dover inserire più di un elemento all'interno dell'array, possiamo comunque effettuare questa operazione tramite l'operatore `$pushAll`, il quale è simile come sintassi alla normale *push*, con la differenza che questa volta il valore specificato all'interno del documento in input all'operatore contiene un array di documenti.
Tenendo come riferimento l'elemento nella collezione `arrays`, vogliamo ora inserire altri 3 numeri all'interno dell'array `a`; il comando da inserire sarà ora il seguente: `db.arrays.update( { _id : 0 } , { $pushAll : { a : [ 7, 8, 9 ] } } )`.

Se dovessimo invece rimuovere un elemento indipendentemente dalla sua posizione all'interno di un array, allora l'operatore da utilizzare è `$pull`, il quale prende sempre un documento come input dentro il quale viene specificato il nome dell'array sul quale operare e l'elemento da eliminare.
Se volessimo dunque rimuovere il numero 5 dall'array `a`, il comando da eseguire è il seguente: `db.arrays.update( { _id : 0 } , { $pull : { a : 5 } } )`.

Ovviamente anche per la *pull* vale il discorso di rimozione di più elementi all'interno di un array, per cui come la *push* ha il suo corrispettivo nell'operatore `$pushAll`, la *pull* ha l'operatore `$pullAll`.
Concettualmente è simile alla normale *pull*, con la differenza che il documento in input prende un array di elementi invece di uno.
Il solito esempio chiarirà la questione: `db.arrays.update( { _id : 0 } , { $pullAll : { a : [ 2, 3, 8 ] } } )`; dal solito array `a` rimuoviamo gli elementi `2`, `3` e `8`.
L'operatore `$pullAll` rimuove ogni occorrenza degli elementi specificati all'interno del suo array, indipendentemente dall'ordine col quale sono salvati. 

Concludendo, se volessimo vedere gli array come contenitori di elementi univoci e avessimo la necessita di inserire un elemento solo, possiamo usare l'operatore `$addToSet`, il quale prende come input un documento simile a quello usato per l'operatore *push*, con la differenza che se l'elemento da inserire è già presente nell'array, questo non viene duplicato, per cui avremo sempre e solo un'unica occorrenza.
Esempio: `db.arrays.update( { _id : 0 } , { $addToSet : { a : 5 } } )`; continuando ad eseguire lo stesso comando vedremo che il campo `a` conterrà sempre e solo una sola occorrenza del numero 5.

Maggiori dettagli sugli operatori `$push`, `$pop`, `$pull`, `$pushAll`, `$pullAll` e `$addToSet` li trovate nelle sezioni dedicate della documentazione ufficiale di MongoDB([push](https://docs.mongodb.com/manual/reference/operator/update/push/), [pop](https://docs.mongodb.com/manual/reference/operator/update/pop/), [pull](https://docs.mongodb.com/manual/reference/operator/update/pull/), [pushAll](https://docs.mongodb.com/manual/reference/operator/update/pushAll/), [pullAll](https://docs.mongodb.com/manual/reference/operator/update/pullAll/), [addToSet](https://docs.mongodb.com/manual/reference/operator/update/addToSet/)).

Bene, abbiamo visto dunque come modificare completamente o parzialmente un documento all'interno di una collezione, per cui possiamo adesso fare un ulteriore step nel capitolo degli *update* e vedere come si comporta MongoDB in caso di **upsert**.
Come forse alcuni di voi sanno già, magari coloro che più hanno avuto a che fare con i database, fare *l'upsert* di un record vuol dire "inserisci se non presente, altrimenti effettua la modifica" (o in parole più semplici: cerca se il record da modificare è già presente nel database; se si allora effettua una normale modifica, altrimenti esegui un nuovo inserimento).
MongoDB mette a disposizione quattro tipi differenti di comportamento per il comando *update*, di cui due li abbiamo già visti; la sintassi completa dell'update è infatti la seguente: `db.<nome_collezione>.update( <condizione_where> , <documento_update> , <documento_opzioni> )`, dove `<documento_opzioni>` (parametro opzionale) contiene le seguenti chiavi-valore: `{ multi : true/false , upsert : true/false , writeConcern : document }`.
Concentriamoci sulla seconda e vediamo infatti che è possibile settare il comportamento di *upsert* alla *update* semplicemente andandogli a settare `true` la rispettiva chiave; basta questo infatti, non è necessario ricordare ulteriori metodi per cambiare il comportamento applicato e, come possiamo ben immaginare, lo stesso vale anche per l'ultimo comportamento che spiegheremo subito dopo.
Facciamo qualche esempio per rendere più chiaro il concetto; creiamo una collezione `pageviews` nel nostro DB di test e proviamo subito a fare un upsert nella collezione in questo modo: `db.pageviews.update( { _id : "/sports/footbal" } , { $inc : { views : 1 } } , { upsert : true } )`.
Eseguiamo l'upsert e vediamo che effettivamente non abbiamo una modifica (anche perchè la collezione è stata appena creata) ma abbiamo ottenuto un inserimento del seguente oggetto: `é "_id" : "/sports/footbal" , "views" : 1 }`; proviamo ora a rieseguire nuovamente il comando update e, se facciamo una find, vediamo che stavolta non è stato effettuato nessun inserimento in quanto esiste già un documento con la chiave `/sports/footbal`, per cui abbiamo un incremento del campo `views` che viene appunto incrementato di uno ogni volta.
Possiamo effettuare un nuovo upsert giusto a titolo di esempio `db.pageviews.update( { _id : "/sports/tennis" } , { $inc : { views : 1 } } , { upsert : true } )` e noteremo che un nuovo inserimento è stato effettuato.
Facciamo ora giusto un paio di considerazioni sulla *upsert*: è importante che la query che eseguiamo per l'update sia "completa", ossia non manchino informazioni per specificare un valore in maniera precisa, altrimenti MongoDB effettuerà un inserimento parziale (se ovviamente non viene trovato il documento da modificare) in quanto tali campi "parziali" semplicemente saranno tagliati fuori e non inseriti nel nuovo documento; se per esempio provassimo a fare il seguente upsert nella nostra collezione `pageviews`: `db.pageviews.update( { views : { $gt : 20 } } , { $set : { "num_clicks" : 50 } } , { upsert : true } )` vedremo che verrà creato un nuovo documento, il quale tuttavia non avrà il campo `views` in quanto non abbiamo specificato un dato preciso, al contrario il campo `num_clicks` esisterà e avrà valore cinquanta (e comunque non verrà valorizzato il campo `_id` come nel primo caso).
Un'ultima nota: per evitare inserimenti multipli dello stesso documento in caso di *upsert*, ricordiamoci sempre di utilizzare campi indicizzati in maniera univoca all'interno del criterio di selezione dell'update (cosa tra l'altro non fatta nell'ultimo esempio di upsert, lol).

Maggiori dettagli sul comportamento di *upsert* della *update* li trovate nella [sezione dedicata della documentazione ufficiale di MongoDB](https://docs.mongodb.com/manual/reference/method/db.collection.update/#upsert-option).

Concludiamo questa sezione con alcune letture interessanti sui vari use case che vedono MongoDB come punto focale e che potete trovare nella sezione [Use Cases](https://www.mongodb.com/use-cases) del sito ufficiale di MongoDB.

Abbiamo parlato di quattro possibili comportamenti dell'*update* e ne abbiamo visti tre.
L'ultimo, come forse avrete già avuto modo di capire, riguarda la possibilità di effettuare modifiche su più documenti tramite il comando `update`.
A differenza del normale `UPDATE` in SQL infatti, l'*update* in MongoDB di default non viene eseguito sull'intera collezione, bensì non appena trova un documento all'interno della collezione che soddisfa il criterio di selezione, l'operazione di modifica si interrompe e ritorna il risultato col numero di documenti modificati (ovviamente uguale a uno).
Qualcuno potrebbe giustamente chiedersi il perchè di questa cosa; beh ripensiamo sempre all'originale scopo di MongoDB, effettivamente viene comodo pensare che ogni oggetto vengé indicizzato e sia unico all'interno di una collezione, per cui era computazionalmente inefficiente effettuare una scansione completa della collezione in quanto l'elemento da modificare era già stato individuato.
Questa cosa se ci pensiamo può valere anche adesso, soprattutto se utilizziamo il più delle volte l'*update* con chiave univoca nel criterio di selezione.
Per "attivare" la possibilità di proseguire sull'intera collezione è necessario quindi settare il comportamento in maniera esplicita, andando a specificare `{ multi : true }` come terzo parametro del comando `update`.
Questo ovviamente nella shell, ma è possibile che su alcuni driver ci sia il metodo dedicato `multiUpdate` che effettui tale modifica.
La sintassi è quindi la seguente: `db.<nome_collezione>.update( <condizione_where> , <documento> , { multi : true } )`.
Facciamo un esempio e riprendiamo la nostra collezione dell'esempio precedente; andiamo ad esempio a incrementare il campo `num_clicks` per ogni elemento all'interno della collezione tramite il seguente comando: `db.pageviews.update( { } , { $inc : { num_clicks : 1 } } , { multi : true } )`.
Eseguiamo il comando e vedremo che tutti i documenti avranno ora il loro campo `num_clicks` incrementato di uno (o settato a uno nel caso non fosse già presente).
Giusto un piccolo appunto su questo esempio: come per la *find*, nell'*update* possiamo selezionare tutti gli elementi all'interno di una collezione tramite il selettore vuoto (`{ }`).

Maggiori dettagli sul comportamento di *multi-update* li trovate nella [sezione dedicata della documentazione ufficiale di MongoDB](https://docs.mongodb.com/manual/reference/method/db.collection.update/#multi-parameter).

Ora che sappiamo come effettuare modifiche su più documenti ricordiamoci però di una cosa fondamentale: MongoDB non ha transazioni e di conseguenza non garandisce l'atomicità delle operazioni che coinvolgono più documenti; al contrario viene garantita l'atomicità sul singolo documento.
Questo è fondamentale perchè a meno di non implementare noi sul codice una gestione di transazioni, non abbiamo la certezza di atomicità "globale" in caso di modifiche che coinvolgoné più di un documento.
Questa è sicuramente una grande limitazione di MongoDB in quei casi in cui vengano trattati più documenti, o record, contemporaneamente e non uno per ogni azione.

Bene, ora che conosciamo buona parte delle possibili interazioni con la shell di MongoDB, l'unica cosa che manca è come rimuovere uno o più documenti da una collezione e come rimuovere delle collezioni intere.
Il comando `remove`, come per tutti quelli visti fino ad ora, sono metodi associati alle collezioni, per cui la sintassi sarà la seguente: `db.<nome_collezione>.remove(<condizione_where>)`, e corrisponde di fatto al comando SQL `DELETE FROM <table> WHERE <condizione_where>`.
Il comportamento della *remove* è praticamente identico a quello della *find*, con la differenza che, beh, cancelliamo dalla collezione i documenti che hanno un match con la condizione di selezione; a tal proposito, forse una delle poche differenze sta nel fatto che mentre con la *find* si può omettere il documento con il criterio di selezione, nella *remove* è obbligatorio, anche se vuoto (`{ }`).
Diversamente dalla *update* invece, la *remove* lavora di default su più documenti ("multi-remove operation"), per cui se mentre per la modifica dovevamo specificare se fare o meno "multi-update", nella *remove* possiamo specificare se effettuare o meno la rimozione di un solo elemento (aggiungendo un documento come secondo parametro dove indichiamo `justOne : true`).
Come per il caso di "multi-update" è importante specificare che la cancellazione di più documenti in serie non è un'operazione atomica, mentre la cancellazione di un singolo documento si; di fatto un client di MongoDB no vedrà mai un documento parziale o troncato.
Facciamo ora un paio di esempi per capire meglio il caso base: possiamo rimuovere un elemento specifico da una collezione `db.products.remove( { "name" : "Tonno in scatola" } )` oppure rimuovere un insieme di documenti in base a un range definito `db.products.remove( { "quantity" : { $gt : 20 } } )`; possiamo eventualmente fare `db.products.remove(<exp>).count()` per conoscere quanti documenti verrebbero affetti dalla cancellazione.

Passiamo ora alla cancellazione di un'intera collezione e distinguiamo innanzitutto il caso in cui si decide di cancellare tutti i documenti di una collezione da quello in cui si vuole effettuare la cancellazione della collezione nel suo insieme (drop).
Nel primo caso possiamo normalmente utilizzare il comando `remove` semplicemente passandogli un documento vuoto (`{ }`), in questo caso l'engine di MongoDB eseguira la rimozione di tutti i documenti all'interno della collezione, uno per uno; nel secondo caso invece basta utilizzare il comando `drop` (anch'esso associato alla collezione) per eliminare in maniera completa una collezione dal database.
Quest'ultima opzione risulta senza dubbio più efficace in termini computazionali, in quanto viene liberata una porzione significativa di memoria in blocco, inclusi i metadati associati, per cui alle volte è anche conveniente effettuare una drop e ricreare nuovamente la collezione da capo piuttosto che iterare su tutti gli elementi da essa contenuti.
La sintassi della *drop* è molto semplice, non prende argomenti e ritorna `true` o `false` in base alla riuscita o meno dell'operazione: `db.<nome_collezione>.drop()`.

Maggiori dettagli sui metodi `remove` e `drop` li trovate nella sezione dedicata della documentazione ufficiale di MongoDB ([remove](https://docs.mongodb.com/manual/reference/method/db.collection.remove/), [drop](https://docs.mongodb.com/manual/reference/method/db.collection.drop/)).

Passiamo ora a un tema cruciale di questo capitolo, che ci permette di rispondere a una domanda che sicuramente molti di voi si saranno posti: ma perchè vedere così tanti comandi é operatori sulla shell di mongo quando di fatto il più delle volte dovro' interagire con MongoDB tramite le API che espone?
La risposta è molto semplice: di fatto la stragrande maggioranza di quello che abbiamo visto fin'ora sulla shell può essere riportato in maniera abbastanza immediata sul codice, tenendo magari qualche piccolo accorgimento.
Come facciamo tuttavia a connetterci all'istanza MongoDB tramite il codice?
Anche qui la risposta è semplice: tramite i driver!
Il driver MongoDB è di fatto una libreria (tendenzialmente più librerie insieme in realtà) che ci mette a disposizione tutta una serie di strumenti e funzionalità volte all'amministrazione e interazione del database; come dicevamo prima infatti è possibile fare più o meno le stesse cose che facevamo con la shell tramite i driver MongoDB.
A partire dalla release 3.0 di MongoDB, per alcuni linguaggi è disponibile anche la versione asincrona del driver.
I driver MongoDB, anch'essi open source, sono disponibili per una buona fetta dei linguaggi maggiormente usati negli ultimi anni, andando quindi dal driver per Node.js, a quello Java, PHP e così via.
La lista completa potete trovarla andando sulla sezione relativa ai [MongoDB Drivers](https://docs.mongodb.com/ecosystem/drivers/) nella documentazione ufficiale.

Andiamo un po' più nel dettaglio e selezioniamo un driver in particolare dal quale poter andare a vedere alcune delle possibili operazioni, rimandando per le restanti alla documentazione ufficiale.
Selezioniamo quindi il driver Java, un po' per comodità, un po' perchè è tra i linguaggi più usati per la programmazione lato back-end (secondo le [statistiche annuali di Stackoverflow](http://stackoverflow.com/research/developer-survey-2016#most-popular-technologies-per-occupation)).
La pagina ufficiale del driver di MongoDB per Java è accessibile a [questo indirizzo](https://docs.mongodb.com/ecosystem/drivers/java/), il quale contiene tutte le informazioni che ci servono per iniziare subito ad utilizzarlo.

Le principali funzionalità del driver Java di MongoDB sono suddivise in quattro grandi tronconi che sono:

- il driver MongoDB vero e proprio, il quale contiene le API utili principalmente per le interazioni tipo CRUD con il database;
- il driver asincrono MongoDB, simile al driver base ma che sfrutta canali asincroni per le comunicazioni non bloccanti ([Netty](http://netty.io/) o [AsynchronousSocketChannel](https://docs.oracle.com/javase/7/docs/api/java/nio/channels/AsynchronousSocketChannel.html));
- la libreria per BSON, di nuova concezione che aiuta per la codifica e decodifica dei documenti;
- il core del driver, ossia quello che sta alla base dei driver sincroni e asincrono, che può essere sfruttato per le operazioni base e per implementare nuove API.

Ne approfittiamo adesso per chiarire subito una questione riguardo il *doppio* driver sincrono e asincrono: questa del *doppio canale di comunicazione* è una particolarità del driver Java (e di alcuni altri driver) di potersi interfacciare in entrambi i modi in base alla effettiva necessità di avere più o meno una risposta immediata da parte del server a seguito di alcune specifiche istruzioni.
Infatti, se in fase di ingegnerizzazione del nostro applicativo poniamo un'attenzione particolare alle performance del sistema, è assolutamente opportuno pensare di diversificare i canali di comunicazione utilizzati e sfruttare da una parte il driver sincrono per tutte quelle azioni che richiedono una risposta (immediata) da parte di MongoDB (come per esempio una *find*), dall'altra sfruttare il driver asincrono per tutte le operazioni per le quali non necessitiamo di una risposta (immediata), le cosiddette azioni "*fire and forget*" (come gli inserimenti per i quali non ci interessa l'indice e le modifiche); ovviamente è importante fare poi differenza tra i tipi degli oggetti ritornati proprio al livello di codice.

Ora che abbiamo chiarito questo concetto introduttivo passiamo alle operazioni che si possono eseguire tramite il driver Java di MongoDB.
Iniziamo innanzitutto con la rappresentazione dei documenti in classi Java.
Come abbiamo visto nel primo capitolo, i documenti vengono salvati in MongoDB attraverso il formato BSON; per la rappresentazione dei documenti in Java avviene più o meno la stessa cosa ma, ovviamente, ci sono diversi modi per rappresentare un documento in una classe.
Uno tra questo è appunto quello di utilizzare la classe [**Document**](http://mongodb.github.io/mongo-java-driver/3.4/javadoc/?org/bson/Document.html), la quale rappresenta una possibile istanza base di un documento appunto.
Andando per esempio a vedere il codice che implementa la classe `Document` vediamo che implementa a sua volta una mappa `Map<String,Object>`, la quale se vogliamo è alla base del concetto *chiave-valore* per il formato JSON; l'oggetto che contiene poi il valore è di tipo `Object`, per cui siamo abbastanza liberi di implementare qualsiasi tipologia di documento attualmente supportata da MongoDB.
Ricordandoci un po' appunto il formato JSON, possiamo costruire un documento utilizzando il metodo `append` fornito proprio dalla classe `Document`, per cui ad esempio un documento che avrà un campo per ogni tipo di dato attualmente supportato da MongoDB sarà il seguente:

```Java
Document document = new Document()
    .append("str", "MongoDB hello")
    .append("int", 42)
    .append("l", 1L)
    .append("double", 1.1)
    .append("bool", false)
    .append("date", new Date())
    .append("_id", new ObjectId())
    .append("null", null)
    .append("embeddedDoc", new Document("x", 0))
    .append("intList", Arrays.asList(1, 2, 3));

System.out.println((String) document.get("str"))
String str = document.getString("str");
int i = document.getInteger("int");
```

La cosa che subito possiamo notare è che in questo caso non c'è un vero e proprio controllo sul tipo ritornato o salvato nel documento, per cui in ogni operazione di scrittura o lettura del documento dobbiamo effettuare un cast esplicito (o implicito nel caso in cui vengano usati dei metodi ad-hoc tipo il `getString` o `getInteger`).
Se invece il nostro applicativo dovesse richiedere una componente assolutamente *typesafe* (controllo garantito del tipo degli oggetti) allora potremmo valutare l'utilizzo di [BsonDocument](http://mongodb.github.io/mongo-java-driver/3.4/javadoc/org/bson/BsonDocument.html), il quale funziona in modo pressochè analogo al `Document` ma con in più una specifica diretta del tipo di dato inserito, a discapito di incisività e stringatezza.
Se andiamo infatti ad analizzare l'implementazione della classe `BsonDocument` notiamo che essa implementa a sua volta sempre una mappa, ma stavolta di tipo `<String,BsonValue>`; quest'ultima è una classe astratta che estende `Object` ed ha come sottoclassi le implementazioni di tutti i possibili tipi di dato supportati da MongoDB, abbiamo infatti [BsonArray](http://mongodb.github.io/mongo-java-driver/3.4/javadoc/org/bson/BsonArray.html), [BsonNumber](http://mongodb.github.io/mongo-java-driver/3.4/javadoc/org/bson/BsonNumber.html), [BsonObjectId](http://mongodb.github.io/mongo-java-driver/3.4/javadoc/org/bson/BsonObjectId.html), ecc.

```Java
BsonDocument bsonDocument = new BsonDocument("str", new BsonString("MongoDB hello"));
```

Infine, c'è un ulteriore modo per salvare i documenti in Java e consiste nell'utilizzo di framework e librerie di terze parti che implementano funzionalità tipo [ORM](https://en.wikipedia.org/wiki/Object-relational_mapping) o [POJO Mapper](https://spring.io/understanding/POJO).
Queste librerie non saranno esplicitamente trattate nei seguenti capitoli, tuttavia per applicazioni enterprise o di grandi dimensioni, a mio avviso, ne è sicuramente suggerito l'utilizzo, in quanto ci facilitano di gran lunga la parte relativa allo sviluppo, a discapito di un minimo di flessibilità relativa proprio ai documenti.
La lista completa dei framework supportati la trovate a [questo indirizzo](https://docs.mongodb.com/ecosystem/drivers/java/#third-party-frameworks-and-libraries) e, per citarne alcuni, sicuramente vale la pena vedere [Morphia](https://github.com/mongodb/morphia) e [Spring MongoDB](http://projects.spring.io/spring-data-mongodb/), il primo è il mapper supportato direttamente da MongoDB, mentre il secondo è il porting per il famoso framework Java (comodissimo proprio perchè integra in maniera ottimale MongoDB e Spring, facilitando di gran lunga il lavoro dello sviluppatore).

Giusto un appunto per questa parte finale del secondo capitolo: tutte le parti che seguono sono prese direttamente dalla guida Quick Start fornita da MongoDB, liberamente accessibile a [questo indirizzo](http://mongodb.github.io/mongo-java-driver/3.4/driver/getting-started/quick-start/).

Bene, ora che sappiamo come vengono rappresentati i documenti di MongoDB in Java iniziamo a presentare tutta una serie di operazioni che possiamo effettuare.
Cominciamo appunto con il vedere come stabilire una connessione da codice a un'istanza MongoDB.
Tale operazione viene effettuata attraverso la classe [MongoClient](http://mongodb.github.io/mongo-java-driver/3.4/javadoc/?com/mongodb/MongoClient.html), il cui costruttore permette appunto di istanziare un pool di connessioni al database al quale vogliamo connetterci.
`MongoClient` può anche essere usata tipo Singleton, in quanto non è neccesario instanziarla più di una volta, anche in caso di applicativi multi-thread.
I dettagli di connessione vengono specificati come parametri al costruttore, come nel seguente modo:

```Java
MongoClient mongoClient = new MongoClient("localhost", 27017);
```

Una volta stabilita la connessione all'istanza di MongoDB possiamo accedere al database specificandone il nome, un po' come facciamo nella shell quando vogliamo cambiare il db (`use <database>`).
Per ottenere il link con il database utilizziamo il metodo [getDatabase](http://mongodb.github.io/mongo-java-driver/3.4/javadoc/?com/mongodb/MongoClient.html#getDatabase-java.lang.String-) di `MongoClient`, specificando il nome del database come nel seguente esempio:

```Java
MongoDatabase database = mongoClient.getDatabase("mydb");
```

L'istanza [MongoDatabase](http://mongodb.github.io/mongo-java-driver/3.4/javadoc/?com/mongodb/MongoClient.html#getDatabase-java.lang.String-) al database che abbiamo appena generato e' immutabile, per cui non possiamo effettuare modifiche al collegamento.

Bene, ora che ci siamo collegati con l'istanza di MongoDB e scelto il dabatase sul quale lavorare, l'unica cosa che ci resta da fare e' collegarci con una collezione (o piu' collezioni se dobbiamo interagire con collezioni diverse nella stessa operazione); per fare cio' usiamo il metodo [getCollection](http://mongodb.github.io/mongo-java-driver/3.4/javadoc/?com/mongodb/client/MongoDatabase.html#getCollection-java.lang.String-) di `MongoDatabase`, il quale prende in input il nome della collezione e ci restituisce il link alla collezione stessa.
Il seguente esempio riassume quanto detto:

```Java
MongoCollection<Document> collection = database.getCollection("test");
```

Come per il database, anche l'istanza MongoCollection alla collezione e' immutabile.

Ora che abbiamo finalmente l'accesso a una collezione possiamo di fatto interagire con essa ed effettuare tutte le operazioni che abbiamo visto prima sulla shell.
Cominciamo quindi con la creazione di un documento come fatta poco fa, come nel seguente modo:

```Java
Document doc = new Document("name", "MongoDB")
    .append("type", "database")
    .append("count", 1)
    .append("versions", Arrays.asList("v3.2", "v3.0", "v2.6"))
    .append("info", new Document("x", 203).append("y", 102));
```

che equivale al seguente JSON:

```JavaScript
{
    "name" : "MongoDB",
    "type" : "database",
    "count" : 1,
    "versions": [ "v3.2", "v3.0", "v2.6" ],
    "info" : {
        x : 203,
        y : 102
    }
}
```

ed effetuiamo un inserimento all'interno della collezione che abbiamo ottenuto in precedenza.
Per fare cio basta semplicemente utilizzare il metodo [insertOne](http://mongodb.github.io/mongo-java-driver/3.4/javadoc/?com/mongodb/client/MongoCollection.html#insertOne-TDocument-), appartenente a `MongoCollection`, che prende appunto come input un documento da salvare (non ritorna nulla), come nel seguente esempio:

```Java
collection.insertOne(doc);
```

E' possibile anche procedere con inserimenti multipli semplicemente utilizzando il metodo [insertMany](http://mongodb.github.io/mongo-java-driver/3.4/javadoc/?com/mongodb/client/MongoCollection.html#insertMany-java.util.List-) che prende in input una lista (`List`) di documenti.
Sia nel caso dell'inserimento singolo che in quello multiplo valgono le stesse regole per la shell, come ad esempio la questione sul campo `_id` che viene inserito automaticamente se non specificato.

Ora che abbiamo un documento all'interno della nostra collezione possiamo cominciare ad interrogarla per ottenere delle informazioni.
La prima cosa che possiamo fare e' contare quanti documenti contiene e possiamo farlo utilizzando il metodo [count](http://mongodb.github.io/mongo-java-driver/3.4/javadoc/com/mongodb/client/MongoCollection.html#count--), come nel seguente esempio:

```Java
long documentsCount = collection.count();
System.out.println(String.valueOf(documentsCount));
```

Sappiamo quindi quanti documenti ci sono nella nostra collezione, ma come fare per ottenerne i dati?
Come per la shell, possiamo ovviamente eseguire una `find`, ma a differenza della shell, in Java otteniamo come risultato un oggetto [FindIterable](http://mongodb.github.io/mongo-java-driver/3.4/javadoc/?com/mongodb/client/FindIterable.html), che rappresenta di fatto un'interfaccia per l'accesso ai dati ritornati e, tramite il metodo [iterator](http://mongodb.github.io/mongo-java-driver/3.4/javadoc/?com/mongodb/client/MongoIterable.html#iterator--) o [first](http://mongodb.github.io/mongo-java-driver/3.4/javadoc/?com/mongodb/client/MongoIterable.html#first--) e' possibile accedere al risultato della `find`.
I seguenti esempi chiariscono quanto appena detto:

```Java
//1
Document myDoc = collection.find().first();
System.out.println(myDoc.toJson());

//2
MongoCursor<Document> cursor = collection.find().iterator();
try {
    while (cursor.hasNext()) {
        System.out.println(cursor.next().toJson());
    }
} finally {
    cursor.close();
}
```

Nel primo esempio vediamo che dalla `find` estraiamo solamente il primo risultato (`first` risulta particolarmente utile infatti quando ci aspettiamo un solo risultato o siamo interessati solo al primo documento tornato), il quale puo' essere associato direttamente a un `Document`.
Nel secondo esempio invece estraiamo un *iteratore* dalla `find`, il quale viene poi associato a un [MongoCursor](http://mongodb.github.io/mongo-java-driver/3.4/javadoc/?com/mongodb/client/MongoIterable.html#iterator--), ossia un'interfaccia che permette l'accesso sequenziale ai dati ottenuti ([hasNext](http://mongodb.github.io/mongo-java-driver/3.4/javadoc/com/mongodb/client/MongoCursor.html#hasNext--) e [next](http://mongodb.github.io/mongo-java-driver/3.4/javadoc/com/mongodb/client/MongoCursor.html#next--)).
Al termine dell'iterazione sul cursore possiamo tranquillamente chiuderlo per liberare spazio allocato.

La `find` tuttavia non esegue solamente - per fortuna - ricerce di collezioni complete e, come abbiamo visto per il caso della shell, possiamo ovviamente specificare dei parametri di ricerca che ci permettono di identificare quei documenti che ci interessano all'interno di una collezione.
Per specificare i parametri di ricerca, e inserire quella che chiamiamo *condizione di WHERE*, dobbiamo passare alla `find` uno o piu' oggetti [Filters](http://mongodb.github.io/mongo-java-driver/3.4/javadoc/?com/mongodb/client/model/Filters.html), i quali ci permettono di costruire la struttura logica della query che vogliamo eseguire.
Se per esempio volessimo ricercare il documento che abbiamo inserito poco fa, il codice da scrivere sara' il seguente:

```Java
Document myDoc = collection.find(eq("type", "database")).first();
System.out.println(myDoc.toJson());
```

[Eq](http://mongodb.github.io/mongo-java-driver/3.4/javadoc/com/mongodb/client/model/Filters.html#eq-TItem-) nell'esempio crea quindi un filtro che equivale all'operatore `$eq` visto sulla shell, e che quindi ma a sondare i documenti di una collezione che rispecchiano il criterio di selezione specificato.
Il driver di MongoDB mette ovviamente a disposizione una folta collezione di filtri, tra cui [and](http://mongodb.github.io/mongo-java-driver/3.4/javadoc/com/mongodb/client/model/Filters.html#and-org.bson.conversions.Bson...-), [or](http://mongodb.github.io/mongo-java-driver/3.4/javadoc/com/mongodb/client/model/Filters.html#or-org.bson.conversions.Bson...-), [gt](http://mongodb.github.io/mongo-java-driver/3.4/javadoc/com/mongodb/client/model/Filters.html#gt-java.lang.String-TItem-) e cosi' via; la lista completa la trovate nella documentazione relativa ai filtri ([qui](http://mongodb.github.io/mongo-java-driver/3.4/javadoc/?com/mongodb/client/model/Filters.html)).

Oltre a inserire e ricercare un documento, tramite le API del driver di MongoDB possiamo ovviamente anche effettuare modifiche e cancellazioni sui documenti delle collezioni.
Possiamo modificare un documento sfruttando il metodo [updateOne](http://mongodb.github.io/mongo-java-driver/3.4/javadoc/?com/mongodb/client/MongoCollection.html#updateOne-org.bson.conversions.Bson-org.bson.conversions.Bson-) messo a disposizione da `MongoCollection`, il quale prende in input due parametri: il primo e' il filtro che serve per identificare il documento da modificare, mentre il secondo e' il documento con le modifiche da apportare.
Ricordiamo le considerazioni fatte per l'update nella shell?
Qui ovviamente valgono le stesse cose, per cui menzioniamo il fatto che di suo l'update sostituisce l'intero documento passato, a meno di non utilizzare operatori come `$set` o `$inc` che permettono di modificare un documento in una collezione senza doverlo sostituire completamente.
Nel caso dovessimo andare a modificare piu' documenti contemporaneamente, sempre tramite `MongoCollection` abbiamo a disposizione il metodo [updateMany](http://mongodb.github.io/mongo-java-driver/3.4/javadoc/?com/mongodb/async/client/MongoCollection.html#updateMany-org.bson.conversions.Bson-org.bson.conversions.Bson-), il quale ha comportamento analogo rispetto all'`updateOne`.
Tenendo sempre in considerazione lo schema del documento inserito poco prima, mettiamo ad esempio di voler incrementare il campo `count` di tutti quei documenti il cui campo `type` sia uguale a "database", il codice da utilizzare sara' il seguente:

```Java
UpdateResult result = collection.updateMany(eq("type", "database"), inc("count", 1));
System.out.println(result.getModifiedCount());
```

Il metodo `updateMany`, come anche l'`updateOne`, ritorna un'istanza dell'oggetto [UpdateResult](http://mongodb.github.io/mongo-java-driver/3.4/javadoc/com/mongodb/client/result/UpdateResult.html), un'abstract che ci mette a disposizione una serie di metodi per interagire col risultato stesso dell'update.

Altra azione che resta da valutare e' la cancellazione di uno o piu' documenti.
La cancellazione e' resa possibile dal driver MongoDB attraverso i metodi `deleteOne` e `deleteMany`, i quali di fatto si comportano come una `find`, ossia entrambi prendono come input dei filtri che servono per descrivere la condizione di selezione.
Entrambe i metodi fanno parte di `MongoCollection`.
I seguenti esempio chiariscono quanto detto:

```Java
//1
collection.deleteOne(eq("type", "database"));

//2
DeleteResult result = collection.deleteMany(gte("count", 10));
System.out.println(result.getDeletedCount());
```

Nel primo esempio vediamo una semplice cancellazione di un documento che abbia come campo `type` uguale a "database", mentre nel secondo esempio rimuoviamo dalla collezione tutti quei documenti con un campo `count` maggiore o uguale a 10.
Notiamo che, come per l'update, anche i metodi per la cancellazione ritornano un'istanza di un oggetto particolare, il [DeleteResult](http://mongodb.github.io/mongo-java-driver/3.4/javadoc/?com/mongodb/client/result/DeleteResult.html), il quale ci mette appunto a disposizione tutta una serie di metodi per interagire con il risultato dell'operazione; in particolare nel secondo esempio andiamo a vedere quanti documenti sono stati effettivamente cancellati dalla nostra collezione.

Bene, ora che abbiamo un ottimo spunto possiamo spaziare quanto piu' possibile sul driver MongoDB, andando a ricercare tutte quelle operazioni che piu' si adattano al nostro caso.
Ovviamente non stiamo qui ad analizzare metodo per metodo, anche perche' delle semplici linee guida iniziali sono piu' che sufficienti per permetterci di capire come utilizzare i costrutti base per Java e andarci poi a cercare i vari oggetti o metodi che ci andranno poi a servire durante le fasi di sviluppo.

E' opportuno tuttavia soffermarsi un attimo su una questione comunque molto importante riguardo l'ambito Java, ossia quello che si incentra sulla mappatura dei documenti con le relative classi.
MongoDB in ambito Java, come per molti altri database sia relazionali che NoSQL in generale, permette l'interfacciamento con librerie di terze parti o framework aggiuntivi, andando a facilitare in un certo senso il compito dello sviluppatore.
Due tra le librerie piu' conosciute e piu' usate per MongoDB sono [Morphia](http://mongodb.github.io/morphia/) e [Spring Data MongoDB](http://projects.spring.io/spring-data-mongodb/).
La prima, **Morphia**, permette un'astrazione *type-safe wrapper con DAO/Datastore*; Morphia infatti effettua un wrap di alcune delle funzionalita' del driver MongoDB incrementandone l'usabilita' e facilitando l'esecuzione di determinate operazioni.
La cosa veramente comoda di Morphia sta nella possibilita' di definire delle entita' di modello utili a creare delle classi che mappano i documenti salvati su MongoDB; tutto questo tramite semplici annotazioni Java; l'accesso e, in generale, la manipolazione di questi oggetti avviene attraverso i cosiddetti `datastore`, artefatti messi a disposizione da Morphia stessa che ci permettono di effettuare tutte le operazioni viste in questa ultima parte del capitolo.
Per ulteriori dettagli e chiarimenti vi invito ovviamente sulla [pagina di documentazione di Morphia](http://mongodb.github.io/morphia/1.2/), nella quale potrete trovare anche utilissimi esempi per capirne al meglio il funzionamento.
Altra libreria, o forse in questo caso sarebbe meglio definirlo framework, e' **Spring Data MongoDB**; questo framework fa parte del progetto piu' grande chiamato [Spring Data](http://projects.spring.io/spring-data/) e permette una gestione *facile* e *consistente* con la programmazione Java basata su Spring.
Spring MongoDB ci mette a disposizione una buona integrazione con i documenti di MongoDB, avendo come punti focali una visione [POJO](https://spring.io/understanding/POJO) dei modelli, o entita', e una interazione con MongoDB e le relative collezioni molto semplice da gestire, soprattutto attraverso il concetto di [repository](http://docs.spring.io/spring-data/data-commons/docs/1.6.1.RELEASE/reference/html/repositories.html) tipico di Spring Data.
Anche qui per ulteriori dettagli e delucidazioni riguardo Spring MongoDB vi rimando alla [pagina principale](http://projects.spring.io/spring-data-mongodb/) di Spring Data MongoDB e alla [relativa documentazione](http://docs.spring.io/spring-data/data-mongo/docs/1.10.0.M1/reference/html/) (versione 1.10.0.M1).

Sia Morphia che Spring Data MongoDB sono entrambe degli ottimi strumenti per lo sviluppo di applicativi che facciano riferimento a un'istanza di MongoDB; il secondo sopratutto se abbiamo gia' a che fare con un'architettura software basata su Spring.
E' importante tuttavia riflettere un attimo sull'utilizzo finale che se ne fara' all'interno del software per capire se conviene piu' uno o l'altro o utilizzare nativamente le API del driver MongoDB.
Innanzitutto bisogna capire l'entita' delle operazioni che andremo ad eseguire: se le interazioni con MongoDB saranno sporadiche e non necessitiamo di una gestione complessa, allora molto probabilmente converra' investire direttamente sulle funzionalita' native offerte dal driver MongoDB, in quanto gia' di suo ci mette a disposizione tutto quello di cui abbiamo bisogno per un'interazione base ma allo stesso tempo molto efficiente.
Questa considerazione vale anche nel caso in cui prevediamo di dover trattare prevalentemente dati non strutturati o documenti dei quali non conosciamo lo schema: gli artefatti nativi del driver come `Document` e `BsonDocument` sono quanto di piu' flessibile e modulabile un programmatore possa sperare di avere nel driver MongoDB, in quanto permettono una liberta massima nella costruzione dei documenti stessi; con Morphia e Spring Data MongoDB questa cosa e' limitata, in quanto se da una parte ci vieme molto comodo avere delle entita' ben definite, dall'altra perdiamo di flessibilita' e se vogliamo avere dei modelli con dei campi liberi dobbiamo strutturare lo schema dei nostri documenti in maniera adeguata.
Col driver nativo ovviamente questo non succede e siamo liberi di strutturare i nostri dati come meglio crediamo.
Se invece siamo interessati prevalentemente alla facilita' di sviluppo e la possibilita' di usare artefatti come i repository o i datastore, allora Morphia e Spring Data MongoDB risultano sicuramente una scelta obbligata, in quanto vanno a ridurre la complessita' degli oggetti da utilizzare.
Altro metro di giudizio e' sicuramente la dimensione del progetto: se andremo a trattare progetti di piccole dimensioni o che comunque non dovranno necessitare di una grossa quantita' di memoria, allora le funzioni native del driver risultano essere una scelta vincente; se invece non abbiamo vincoli allora Morphia e Spring Data MongoDB risultano sicuramente una buona scelta.
Ultima nota: Morphia e' direttamente supportato da MongoDB Inc. ossia sono gli stessi sviluppatori di MongoDB che lavorano su Morphia, mentre potrebbe non essere la stessa cosa per quanto riguarda Spring Data MongoDB (nda: in realta' essendo entrambi open-souce molto probabilmente saranno proprio le stesse persone che, una volta terminata una versione iniziale di Morphia, abbiano deciso bene di costruire un modulo per Spring).

Bene.
Si conclude qui questo capitolo sulle operazioni di CRUD per MongoDB, dove abbiamo visto tutte le funzionalita' su mongo shell e il corrispettivo su driver Java.