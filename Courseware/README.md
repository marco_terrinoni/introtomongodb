# Introduction Course to MongoDB

## This is part of the MongoDB course in Consoft Sistemi S.p.A.

Author: [Marco Terrinoni](mailto:marco.terrinoni@consoft.it)

This folder contains the links to the slides used during the course, plus a summary for each chapter.
