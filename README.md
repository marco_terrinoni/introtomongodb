# Introduction Course to MongoDB

![MongoDB](https://worldvectorlogo.com/logos/mongodb.svg)

## This is part of the MongoDB course in Consoft Sistemi S.p.A.

Author: [Marco Terrinoni](mailto:marco.terrinoni@consoft.it)

### Extended topics list
1. Introduction
    * Introduction to NoSQL World
    * What is MongoDB
    * MongoDB: Relative to Relational
    * The Nexus Architecture
    * Introduction to JSON
    * JSON Revisited
    * Document Overview
    * Binary JSON (BSON) and Applications
    * MongoDB is Schemaless (Dynamic Schema)
    * System Requirements (32 Vs. 64 Bit)
    * Installing MongoDB
    * The MongoDB Shell
    * Overview of Building an Application with MongoDB

2. CRUD
    * CRUD and the Mongo Shell
    * Inserting Documents
    * Introduction to `findOne`
    * Introduction to `find`
    * Querying Using Field Selection
    * Querying Using `$gt` and `$lt`
    * Inequalities on Strings
    * Using Regular Expressions, `$exists`, and `$type`
    * Using `$and`, `$or`, `$not`, `$nor`
    * Querying Inside Arrays (`$in`, `$all`)
    * Queries with Dot Notation
    * Cursors
    * Counting Results
    * Wholesale Updating of a Document
    * Using the `$set` and `$unset` Commands
    * Using `$push`, `$pull`, `$pushAll`, `$pullAll`, `$addToSet`
    * Upserts
    * Multi-Update
    * Removing Data
    * The MongoDB Driver (Java)
        * representing documents
        * connection
        * insert
        * find, find one, count
        * querying with a filter
        * update and replace
        * delete
    * TODO: insert or merge some topics from M102

3. Schema Design & Performance
    * Relational Normalization
    * MongoDB Schema Design
        * blog example
    * Living Without Constraints
    * Living Without Transactions
    * Relations
        * one-to-one
        * one-to-many
        * many-to-many
    * Benefits of Embedding
        * blog example encore
    * When to Denormalize
    * What is an ODM
    * MORPHIA and Friends
    * Storage Engines
        * introduction
        * WiredTiger (default)
        * Encrypted storage engine
        * In-Memory storage engine
        * MMAPv1
    * Indexes
        * shell commands for indexes (`createIndex()`, `getIndexes()`, `dropIndex()`)
        * index notes
        * unique indexes
        * multi-key indexes
        * sparse indexes
        * TTL indexes
        * Geospatial indexes
        * text indexes
        * background index creation
    * Explain Plans
    * Covered Queries
    * Efficiency of Index Use
    * Logging Slow Queries
    * The Profiler (TODO: decide if necessary)

4. Aggregation Framework & Introduction to Replication and Sharding
    * The Aggregation Pipeline
    * Compound Grouping
    * Using a Document for \_id
    * Aggregation Expressions
    * Aggregation Operators
        * `$sum`
        * `$avg`
        * `$addToSet`
        * `$push`
        * `$max` and `$min`
        * `$group`
        * `$project`
        * `$match`
        * `$sort`
        * `$limit` and `$skip`
        * `$first` and `$last`
        * `$unwind`
    * Mapping Between SQL and Aggregation
        * some common SQL examples
    * Limitation of the Aggregation Framework
    * Aggregation Framework and the Java Driver
    * Introduction to Replication
    * Replica Set in MongoDB
        * connecting to a Replica Set from the Java Driver
    * Read Preferences
    * Review of Implications of Replication
    * Introduction to Sharding
    * Building a Sharded Environment
    * Implication of Sharding
    * Sharding + Replica Set
    * Choosing a Shard Key

5. Replication and Sharding & Introduction to Backup and Recovery
    * Replication Overview
    * Asynchronous Replication
    * Replication Concepts
    * Starting and Initiating a Replica Set
    * Reading and Writing preferences on a Replica Set
    * Failover
    * Arbiters, Priority, and Voting
    * Write Concern Use Case & Patterns
    * Replica Sets and Storage Engine Considerations
    * Sharding and Data Distribution
    * Replication with Sharding
    * Sharding Processes
    * Cluster Topology
    * Working with a Sharded Cluster
    * Cardinality and Monotonic Shard Keys
    * Shard Key Selection
    * Further Tips and Best Practice
    * Overview of Security
    * Overview of Backing Up
    * Backup Strategies
    * Additional Features of MongoDB
    * GridFS
    * Hardware Tips
    * Additional Resources
